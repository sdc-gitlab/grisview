## GRISView

GRISView is an upcoming visualization and analysis tool to work with calibrated GREGOR/GRIS observational datasets. It is intended to facilitate easy data preview and provide interactive tools for quick plotting, analysis and exporting. Tested features in current release include:

- Advanced view, pan and zoom functions for map images and spectra
- Both single map and time-series observations support
- Multiple POI (point-of-interest) and ROI (rectangle-of-interest) to study spatial features
- Interactive map contours generation and profile cuts along given direction
- Distance measurements between map points in different units
- Relative wavelength scale for quick wavelength difference evaluation
- Supports only data format that is distributed by the SDC web archive
- Exporting spectral and map plot view areas as images
- Showing input and derived Stokes parameter plots with Quiet Sun continuum normalization
- Saving and restoring working sessions

GRISView is written entirely in Python with GUI using Qt cross-platform framework.

If you have any questions please contact: yakobchuk@leibniz-kis.de

## Installation

You can install GRISView from the GitLab repo directly using pip
```
pip install git+https://gitlab.leibniz-kis.de/sdc/gris/grisview.git
```

or clone the repo and run setup
```
git clone https://gitlab.leibniz-kis.de/sdc/gris/grisview.git
cd grisview
python setup.py install
```

## Usage

You can start GRISView GUI from command-line in Python environment
(that it was intalled to) as:

```
grisview [-h] [file/folder]

Options:
  -h            Show help message and exit
  file/folder   GRIS observation folder or any FITS file inside it
```
