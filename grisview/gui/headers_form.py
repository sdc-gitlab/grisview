# Form implementation generated from reading ui file 'headers_form.ui'
#
# Created by: PyQt6 UI code generator 6.4.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic6 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt6 import QtCore, QtGui, QtWidgets


class Ui_GrisViewHeaders(object):
    def setupUi(self, GrisViewHeaders):
        GrisViewHeaders.setObjectName("GrisViewHeaders")
        GrisViewHeaders.resize(688, 801)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(GrisViewHeaders)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label12 = QtWidgets.QLabel(parent=GrisViewHeaders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label12.sizePolicy().hasHeightForWidth())
        self.label12.setSizePolicy(sizePolicy)
        self.label12.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label12.setObjectName("label12")
        self.horizontalLayout_3.addWidget(self.label12)
        self.lblPath = QtWidgets.QLineEdit(parent=GrisViewHeaders)
        self.lblPath.setMinimumSize(QtCore.QSize(0, 30))
        self.lblPath.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lblPath.setStyleSheet("padding-left: 4px;")
        self.lblPath.setReadOnly(True)
        self.lblPath.setObjectName("lblPath")
        self.horizontalLayout_3.addWidget(self.lblPath)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_41 = QtWidgets.QLabel(parent=GrisViewHeaders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_41.sizePolicy().hasHeightForWidth())
        self.label_41.setSizePolicy(sizePolicy)
        self.label_41.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label_41.setObjectName("label_41")
        self.horizontalLayout_2.addWidget(self.label_41)
        self.lblFile = QtWidgets.QLineEdit(parent=GrisViewHeaders)
        self.lblFile.setMinimumSize(QtCore.QSize(0, 30))
        self.lblFile.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lblFile.setStyleSheet("padding-left: 4px;")
        self.lblFile.setReadOnly(True)
        self.lblFile.setObjectName("lblFile")
        self.horizontalLayout_2.addWidget(self.lblFile)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setSpacing(4)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.panelMaps = QtWidgets.QFrame(parent=GrisViewHeaders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.panelMaps.sizePolicy().hasHeightForWidth())
        self.panelMaps.setSizePolicy(sizePolicy)
        self.panelMaps.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.panelMaps.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.panelMaps.setObjectName("panelMaps")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.panelMaps)
        self.horizontalLayout_6.setContentsMargins(0, 2, 0, 2)
        self.horizontalLayout_6.setSpacing(4)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label3_2 = QtWidgets.QLabel(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label3_2.sizePolicy().hasHeightForWidth())
        self.label3_2.setSizePolicy(sizePolicy)
        self.label3_2.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label3_2.setObjectName("label3_2")
        self.horizontalLayout_6.addWidget(self.label3_2)
        self.btnFirstMap = QtWidgets.QPushButton(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnFirstMap.sizePolicy().hasHeightForWidth())
        self.btnFirstMap.setSizePolicy(sizePolicy)
        self.btnFirstMap.setMinimumSize(QtCore.QSize(26, 26))
        self.btnFirstMap.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnFirstMap.setFont(font)
        self.btnFirstMap.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnFirstMap.setAutoFillBackground(False)
        self.btnFirstMap.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnFirstMap.setCheckable(False)
        self.btnFirstMap.setAutoExclusive(False)
        self.btnFirstMap.setFlat(False)
        self.btnFirstMap.setObjectName("btnFirstMap")
        self.horizontalLayout_6.addWidget(self.btnFirstMap)
        self.btnPrevMap = QtWidgets.QPushButton(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnPrevMap.sizePolicy().hasHeightForWidth())
        self.btnPrevMap.setSizePolicy(sizePolicy)
        self.btnPrevMap.setMinimumSize(QtCore.QSize(26, 26))
        self.btnPrevMap.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnPrevMap.setFont(font)
        self.btnPrevMap.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnPrevMap.setAutoFillBackground(False)
        self.btnPrevMap.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnPrevMap.setCheckable(False)
        self.btnPrevMap.setAutoExclusive(False)
        self.btnPrevMap.setFlat(False)
        self.btnPrevMap.setObjectName("btnPrevMap")
        self.horizontalLayout_6.addWidget(self.btnPrevMap)
        self.cboMap = QtWidgets.QComboBox(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Preferred, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cboMap.sizePolicy().hasHeightForWidth())
        self.cboMap.setSizePolicy(sizePolicy)
        self.cboMap.setMinimumSize(QtCore.QSize(54, 26))
        self.cboMap.setMaximumSize(QtCore.QSize(54, 26))
        self.cboMap.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.cboMap.setLayoutDirection(QtCore.Qt.LayoutDirection.LeftToRight)
        self.cboMap.setAutoFillBackground(False)
        self.cboMap.setStyleSheet("")
        self.cboMap.setEditable(True)
        self.cboMap.setMaxVisibleItems(20)
        self.cboMap.setSizeAdjustPolicy(QtWidgets.QComboBox.SizeAdjustPolicy.AdjustToContents)
        self.cboMap.setFrame(True)
        self.cboMap.setObjectName("cboMap")
        self.cboMap.addItem("")
        self.cboMap.addItem("")
        self.cboMap.addItem("")
        self.cboMap.addItem("")
        self.cboMap.addItem("")
        self.cboMap.addItem("")
        self.cboMap.addItem("")
        self.horizontalLayout_6.addWidget(self.cboMap)
        self.label4_3 = QtWidgets.QLabel(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label4_3.sizePolicy().hasHeightForWidth())
        self.label4_3.setSizePolicy(sizePolicy)
        self.label4_3.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label4_3.setObjectName("label4_3")
        self.horizontalLayout_6.addWidget(self.label4_3)
        self.lblMapNum = QtWidgets.QLabel(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblMapNum.sizePolicy().hasHeightForWidth())
        self.lblMapNum.setSizePolicy(sizePolicy)
        self.lblMapNum.setMinimumSize(QtCore.QSize(54, 0))
        self.lblMapNum.setMaximumSize(QtCore.QSize(54, 26))
        self.lblMapNum.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.lblMapNum.setFrameShadow(QtWidgets.QFrame.Shadow.Sunken)
        self.lblMapNum.setLineWidth(1)
        self.lblMapNum.setMidLineWidth(0)
        self.lblMapNum.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.lblMapNum.setObjectName("lblMapNum")
        self.horizontalLayout_6.addWidget(self.lblMapNum)
        self.btnNextMap = QtWidgets.QPushButton(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnNextMap.sizePolicy().hasHeightForWidth())
        self.btnNextMap.setSizePolicy(sizePolicy)
        self.btnNextMap.setMinimumSize(QtCore.QSize(26, 26))
        self.btnNextMap.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnNextMap.setFont(font)
        self.btnNextMap.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnNextMap.setAutoFillBackground(False)
        self.btnNextMap.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnNextMap.setCheckable(False)
        self.btnNextMap.setAutoExclusive(False)
        self.btnNextMap.setFlat(False)
        self.btnNextMap.setObjectName("btnNextMap")
        self.horizontalLayout_6.addWidget(self.btnNextMap)
        self.btnLastMap = QtWidgets.QPushButton(parent=self.panelMaps)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnLastMap.sizePolicy().hasHeightForWidth())
        self.btnLastMap.setSizePolicy(sizePolicy)
        self.btnLastMap.setMinimumSize(QtCore.QSize(26, 26))
        self.btnLastMap.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnLastMap.setFont(font)
        self.btnLastMap.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnLastMap.setAutoFillBackground(False)
        self.btnLastMap.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnLastMap.setCheckable(False)
        self.btnLastMap.setAutoExclusive(False)
        self.btnLastMap.setFlat(False)
        self.btnLastMap.setObjectName("btnLastMap")
        self.horizontalLayout_6.addWidget(self.btnLastMap)
        self.horizontalLayout_5.addWidget(self.panelMaps)
        self.panelSlits = QtWidgets.QFrame(parent=GrisViewHeaders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.panelSlits.sizePolicy().hasHeightForWidth())
        self.panelSlits.setSizePolicy(sizePolicy)
        self.panelSlits.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.panelSlits.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.panelSlits.setObjectName("panelSlits")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.panelSlits)
        self.horizontalLayout.setContentsMargins(0, 2, 0, 2)
        self.horizontalLayout.setSpacing(4)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label3 = QtWidgets.QLabel(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label3.sizePolicy().hasHeightForWidth())
        self.label3.setSizePolicy(sizePolicy)
        self.label3.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label3.setObjectName("label3")
        self.horizontalLayout.addWidget(self.label3)
        self.btnFirstSlit = QtWidgets.QPushButton(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnFirstSlit.sizePolicy().hasHeightForWidth())
        self.btnFirstSlit.setSizePolicy(sizePolicy)
        self.btnFirstSlit.setMinimumSize(QtCore.QSize(26, 26))
        self.btnFirstSlit.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnFirstSlit.setFont(font)
        self.btnFirstSlit.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnFirstSlit.setAutoFillBackground(False)
        self.btnFirstSlit.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnFirstSlit.setCheckable(False)
        self.btnFirstSlit.setAutoExclusive(False)
        self.btnFirstSlit.setFlat(False)
        self.btnFirstSlit.setObjectName("btnFirstSlit")
        self.horizontalLayout.addWidget(self.btnFirstSlit)
        self.btnPrevSlit = QtWidgets.QPushButton(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnPrevSlit.sizePolicy().hasHeightForWidth())
        self.btnPrevSlit.setSizePolicy(sizePolicy)
        self.btnPrevSlit.setMinimumSize(QtCore.QSize(26, 26))
        self.btnPrevSlit.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnPrevSlit.setFont(font)
        self.btnPrevSlit.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnPrevSlit.setAutoFillBackground(False)
        self.btnPrevSlit.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnPrevSlit.setCheckable(False)
        self.btnPrevSlit.setAutoExclusive(False)
        self.btnPrevSlit.setFlat(False)
        self.btnPrevSlit.setObjectName("btnPrevSlit")
        self.horizontalLayout.addWidget(self.btnPrevSlit)
        self.cboSlit = QtWidgets.QComboBox(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Preferred, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cboSlit.sizePolicy().hasHeightForWidth())
        self.cboSlit.setSizePolicy(sizePolicy)
        self.cboSlit.setMinimumSize(QtCore.QSize(54, 26))
        self.cboSlit.setMaximumSize(QtCore.QSize(54, 26))
        self.cboSlit.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.cboSlit.setLayoutDirection(QtCore.Qt.LayoutDirection.LeftToRight)
        self.cboSlit.setAutoFillBackground(False)
        self.cboSlit.setStyleSheet("")
        self.cboSlit.setEditable(True)
        self.cboSlit.setMaxVisibleItems(20)
        self.cboSlit.setSizeAdjustPolicy(QtWidgets.QComboBox.SizeAdjustPolicy.AdjustToContents)
        self.cboSlit.setFrame(True)
        self.cboSlit.setObjectName("cboSlit")
        self.cboSlit.addItem("")
        self.cboSlit.addItem("")
        self.cboSlit.addItem("")
        self.cboSlit.addItem("")
        self.cboSlit.addItem("")
        self.cboSlit.addItem("")
        self.cboSlit.addItem("")
        self.horizontalLayout.addWidget(self.cboSlit)
        self.label4 = QtWidgets.QLabel(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label4.sizePolicy().hasHeightForWidth())
        self.label4.setSizePolicy(sizePolicy)
        self.label4.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label4.setObjectName("label4")
        self.horizontalLayout.addWidget(self.label4)
        self.lblSlitNum = QtWidgets.QLabel(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblSlitNum.sizePolicy().hasHeightForWidth())
        self.lblSlitNum.setSizePolicy(sizePolicy)
        self.lblSlitNum.setMinimumSize(QtCore.QSize(54, 0))
        self.lblSlitNum.setMaximumSize(QtCore.QSize(54, 26))
        self.lblSlitNum.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.lblSlitNum.setFrameShadow(QtWidgets.QFrame.Shadow.Sunken)
        self.lblSlitNum.setLineWidth(1)
        self.lblSlitNum.setMidLineWidth(0)
        self.lblSlitNum.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.lblSlitNum.setObjectName("lblSlitNum")
        self.horizontalLayout.addWidget(self.lblSlitNum)
        self.btnNextSlit = QtWidgets.QPushButton(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnNextSlit.sizePolicy().hasHeightForWidth())
        self.btnNextSlit.setSizePolicy(sizePolicy)
        self.btnNextSlit.setMinimumSize(QtCore.QSize(26, 26))
        self.btnNextSlit.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnNextSlit.setFont(font)
        self.btnNextSlit.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnNextSlit.setAutoFillBackground(False)
        self.btnNextSlit.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnNextSlit.setCheckable(False)
        self.btnNextSlit.setAutoExclusive(False)
        self.btnNextSlit.setFlat(False)
        self.btnNextSlit.setObjectName("btnNextSlit")
        self.horizontalLayout.addWidget(self.btnNextSlit)
        self.btnLastSlit = QtWidgets.QPushButton(parent=self.panelSlits)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnLastSlit.sizePolicy().hasHeightForWidth())
        self.btnLastSlit.setSizePolicy(sizePolicy)
        self.btnLastSlit.setMinimumSize(QtCore.QSize(26, 26))
        self.btnLastSlit.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnLastSlit.setFont(font)
        self.btnLastSlit.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnLastSlit.setAutoFillBackground(False)
        self.btnLastSlit.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnLastSlit.setCheckable(False)
        self.btnLastSlit.setAutoExclusive(False)
        self.btnLastSlit.setFlat(False)
        self.btnLastSlit.setObjectName("btnLastSlit")
        self.horizontalLayout.addWidget(self.btnLastSlit)
        self.horizontalLayout_5.addWidget(self.panelSlits)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.panelFiles = QtWidgets.QFrame(parent=GrisViewHeaders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.panelFiles.sizePolicy().hasHeightForWidth())
        self.panelFiles.setSizePolicy(sizePolicy)
        self.panelFiles.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.panelFiles.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.panelFiles.setObjectName("panelFiles")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout(self.panelFiles)
        self.horizontalLayout_8.setContentsMargins(0, 2, 0, 2)
        self.horizontalLayout_8.setSpacing(4)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.label3_4 = QtWidgets.QLabel(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label3_4.sizePolicy().hasHeightForWidth())
        self.label3_4.setSizePolicy(sizePolicy)
        self.label3_4.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label3_4.setObjectName("label3_4")
        self.horizontalLayout_8.addWidget(self.label3_4)
        self.btnFirstFile = QtWidgets.QPushButton(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnFirstFile.sizePolicy().hasHeightForWidth())
        self.btnFirstFile.setSizePolicy(sizePolicy)
        self.btnFirstFile.setMinimumSize(QtCore.QSize(26, 26))
        self.btnFirstFile.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnFirstFile.setFont(font)
        self.btnFirstFile.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnFirstFile.setAutoFillBackground(False)
        self.btnFirstFile.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnFirstFile.setCheckable(False)
        self.btnFirstFile.setAutoExclusive(False)
        self.btnFirstFile.setFlat(False)
        self.btnFirstFile.setObjectName("btnFirstFile")
        self.horizontalLayout_8.addWidget(self.btnFirstFile)
        self.btnPrevFile = QtWidgets.QPushButton(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnPrevFile.sizePolicy().hasHeightForWidth())
        self.btnPrevFile.setSizePolicy(sizePolicy)
        self.btnPrevFile.setMinimumSize(QtCore.QSize(26, 26))
        self.btnPrevFile.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnPrevFile.setFont(font)
        self.btnPrevFile.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnPrevFile.setAutoFillBackground(False)
        self.btnPrevFile.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnPrevFile.setCheckable(False)
        self.btnPrevFile.setAutoExclusive(False)
        self.btnPrevFile.setFlat(False)
        self.btnPrevFile.setObjectName("btnPrevFile")
        self.horizontalLayout_8.addWidget(self.btnPrevFile)
        self.cboFile = QtWidgets.QComboBox(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Preferred, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cboFile.sizePolicy().hasHeightForWidth())
        self.cboFile.setSizePolicy(sizePolicy)
        self.cboFile.setMinimumSize(QtCore.QSize(54, 26))
        self.cboFile.setMaximumSize(QtCore.QSize(54, 26))
        self.cboFile.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.cboFile.setLayoutDirection(QtCore.Qt.LayoutDirection.LeftToRight)
        self.cboFile.setAutoFillBackground(False)
        self.cboFile.setStyleSheet("")
        self.cboFile.setEditable(True)
        self.cboFile.setMaxVisibleItems(20)
        self.cboFile.setSizeAdjustPolicy(QtWidgets.QComboBox.SizeAdjustPolicy.AdjustToContents)
        self.cboFile.setFrame(True)
        self.cboFile.setObjectName("cboFile")
        self.cboFile.addItem("")
        self.cboFile.addItem("")
        self.cboFile.addItem("")
        self.cboFile.addItem("")
        self.cboFile.addItem("")
        self.cboFile.addItem("")
        self.cboFile.addItem("")
        self.horizontalLayout_8.addWidget(self.cboFile)
        self.label4_4 = QtWidgets.QLabel(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label4_4.sizePolicy().hasHeightForWidth())
        self.label4_4.setSizePolicy(sizePolicy)
        self.label4_4.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label4_4.setObjectName("label4_4")
        self.horizontalLayout_8.addWidget(self.label4_4)
        self.lblFileNum = QtWidgets.QLabel(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblFileNum.sizePolicy().hasHeightForWidth())
        self.lblFileNum.setSizePolicy(sizePolicy)
        self.lblFileNum.setMinimumSize(QtCore.QSize(54, 0))
        self.lblFileNum.setMaximumSize(QtCore.QSize(54, 26))
        self.lblFileNum.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.lblFileNum.setFrameShadow(QtWidgets.QFrame.Shadow.Sunken)
        self.lblFileNum.setLineWidth(1)
        self.lblFileNum.setMidLineWidth(0)
        self.lblFileNum.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.lblFileNum.setObjectName("lblFileNum")
        self.horizontalLayout_8.addWidget(self.lblFileNum)
        self.btnNextFile = QtWidgets.QPushButton(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnNextFile.sizePolicy().hasHeightForWidth())
        self.btnNextFile.setSizePolicy(sizePolicy)
        self.btnNextFile.setMinimumSize(QtCore.QSize(26, 26))
        self.btnNextFile.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnNextFile.setFont(font)
        self.btnNextFile.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnNextFile.setAutoFillBackground(False)
        self.btnNextFile.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnNextFile.setCheckable(False)
        self.btnNextFile.setAutoExclusive(False)
        self.btnNextFile.setFlat(False)
        self.btnNextFile.setObjectName("btnNextFile")
        self.horizontalLayout_8.addWidget(self.btnNextFile)
        self.btnLastFile = QtWidgets.QPushButton(parent=self.panelFiles)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnLastFile.sizePolicy().hasHeightForWidth())
        self.btnLastFile.setSizePolicy(sizePolicy)
        self.btnLastFile.setMinimumSize(QtCore.QSize(26, 26))
        self.btnLastFile.setMaximumSize(QtCore.QSize(26, 26))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        self.btnLastFile.setFont(font)
        self.btnLastFile.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.btnLastFile.setAutoFillBackground(False)
        self.btnLastFile.setStyleSheet("QPushButton:checked { background-color: rgb(62, 68, 81) ; color: rgb(247, 247, 247) }")
        self.btnLastFile.setCheckable(False)
        self.btnLastFile.setAutoExclusive(False)
        self.btnLastFile.setFlat(False)
        self.btnLastFile.setObjectName("btnLastFile")
        self.horizontalLayout_8.addWidget(self.btnLastFile)
        self.horizontalLayout_4.addWidget(self.panelFiles)
        self.frame = QtWidgets.QFrame(parent=GrisViewHeaders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_7.setSpacing(4)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label3_3 = QtWidgets.QLabel(parent=self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label3_3.sizePolicy().hasHeightForWidth())
        self.label3_3.setSizePolicy(sizePolicy)
        self.label3_3.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeading|QtCore.Qt.AlignmentFlag.AlignLeft|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.label3_3.setObjectName("label3_3")
        self.horizontalLayout_7.addWidget(self.label3_3)
        self.editFilterKeywords = QtWidgets.QLineEdit(parent=self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.editFilterKeywords.sizePolicy().hasHeightForWidth())
        self.editFilterKeywords.setSizePolicy(sizePolicy)
        self.editFilterKeywords.setMinimumSize(QtCore.QSize(150, 26))
        self.editFilterKeywords.setMaximumSize(QtCore.QSize(150, 26))
        self.editFilterKeywords.setObjectName("editFilterKeywords")
        self.horizontalLayout_7.addWidget(self.editFilterKeywords)
        self.btnClearFilter = QtWidgets.QPushButton(parent=self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnClearFilter.sizePolicy().hasHeightForWidth())
        self.btnClearFilter.setSizePolicy(sizePolicy)
        self.btnClearFilter.setMinimumSize(QtCore.QSize(70, 26))
        self.btnClearFilter.setMaximumSize(QtCore.QSize(70, 26))
        self.btnClearFilter.setObjectName("btnClearFilter")
        self.horizontalLayout_7.addWidget(self.btnClearFilter)
        self.horizontalLayout_4.addWidget(self.frame)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.wgtHeader = TreeWidget(parent=GrisViewHeaders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Preferred, QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wgtHeader.sizePolicy().hasHeightForWidth())
        self.wgtHeader.setSizePolicy(sizePolicy)
        self.wgtHeader.setObjectName("wgtHeader")
        self.verticalLayout_2.addWidget(self.wgtHeader)

        self.retranslateUi(GrisViewHeaders)
        self.cboMap.setCurrentIndex(0)
        self.cboSlit.setCurrentIndex(0)
        self.cboFile.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(GrisViewHeaders)

    def retranslateUi(self, GrisViewHeaders):
        _translate = QtCore.QCoreApplication.translate
        GrisViewHeaders.setWindowTitle(_translate("GrisViewHeaders", "View Headers"))
        self.label12.setText(_translate("GrisViewHeaders", "Directory"))
        self.lblPath.setText(_translate("GrisViewHeaders", "gris_20170928_130033_l1p_007_001_0070.fits"))
        self.label_41.setText(_translate("GrisViewHeaders", "File"))
        self.lblFile.setText(_translate("GrisViewHeaders", "gris_20170928_130033_l1p_007_001_0070.fits"))
        self.label3_2.setText(_translate("GrisViewHeaders", "Map"))
        self.btnFirstMap.setText(_translate("GrisViewHeaders", "⇤"))
        self.btnFirstMap.setShortcut(_translate("GrisViewHeaders", "Ctrl+Home"))
        self.btnPrevMap.setText(_translate("GrisViewHeaders", "←"))
        self.btnPrevMap.setShortcut(_translate("GrisViewHeaders", "Ctrl+Left"))
        self.cboMap.setCurrentText(_translate("GrisViewHeaders", "  1"))
        self.cboMap.setItemText(0, _translate("GrisViewHeaders", "  1"))
        self.cboMap.setItemText(1, _translate("GrisViewHeaders", "  3"))
        self.cboMap.setItemText(2, _translate("GrisViewHeaders", "  5"))
        self.cboMap.setItemText(3, _translate("GrisViewHeaders", "  9"))
        self.cboMap.setItemText(4, _translate("GrisViewHeaders", "  16"))
        self.cboMap.setItemText(5, _translate("GrisViewHeaders", "  25"))
        self.cboMap.setItemText(6, _translate("GrisViewHeaders", "  36"))
        self.label4_3.setText(_translate("GrisViewHeaders", "of"))
        self.lblMapNum.setText(_translate("GrisViewHeaders", "45"))
        self.btnNextMap.setText(_translate("GrisViewHeaders", "→"))
        self.btnNextMap.setShortcut(_translate("GrisViewHeaders", "Ctrl+Right"))
        self.btnLastMap.setText(_translate("GrisViewHeaders", "⇥"))
        self.btnLastMap.setShortcut(_translate("GrisViewHeaders", "Ctrl+End"))
        self.label3.setText(_translate("GrisViewHeaders", "Slit"))
        self.btnFirstSlit.setText(_translate("GrisViewHeaders", "⇤"))
        self.btnFirstSlit.setShortcut(_translate("GrisViewHeaders", "Home"))
        self.btnPrevSlit.setText(_translate("GrisViewHeaders", "←"))
        self.btnPrevSlit.setShortcut(_translate("GrisViewHeaders", "Left"))
        self.cboSlit.setCurrentText(_translate("GrisViewHeaders", "  1"))
        self.cboSlit.setItemText(0, _translate("GrisViewHeaders", "  1"))
        self.cboSlit.setItemText(1, _translate("GrisViewHeaders", "  3"))
        self.cboSlit.setItemText(2, _translate("GrisViewHeaders", "  5"))
        self.cboSlit.setItemText(3, _translate("GrisViewHeaders", "  9"))
        self.cboSlit.setItemText(4, _translate("GrisViewHeaders", "  16"))
        self.cboSlit.setItemText(5, _translate("GrisViewHeaders", "  25"))
        self.cboSlit.setItemText(6, _translate("GrisViewHeaders", "  36"))
        self.label4.setText(_translate("GrisViewHeaders", "of"))
        self.lblSlitNum.setText(_translate("GrisViewHeaders", "45"))
        self.btnNextSlit.setText(_translate("GrisViewHeaders", "→"))
        self.btnNextSlit.setShortcut(_translate("GrisViewHeaders", "Right"))
        self.btnLastSlit.setText(_translate("GrisViewHeaders", "⇥"))
        self.btnLastSlit.setShortcut(_translate("GrisViewHeaders", "End"))
        self.label3_4.setText(_translate("GrisViewHeaders", "File"))
        self.btnFirstFile.setText(_translate("GrisViewHeaders", "⇤"))
        self.btnFirstFile.setShortcut(_translate("GrisViewHeaders", "Ctrl+Home"))
        self.btnPrevFile.setText(_translate("GrisViewHeaders", "←"))
        self.btnPrevFile.setShortcut(_translate("GrisViewHeaders", "Ctrl+Left"))
        self.cboFile.setCurrentText(_translate("GrisViewHeaders", "  1"))
        self.cboFile.setItemText(0, _translate("GrisViewHeaders", "  1"))
        self.cboFile.setItemText(1, _translate("GrisViewHeaders", "  3"))
        self.cboFile.setItemText(2, _translate("GrisViewHeaders", "  5"))
        self.cboFile.setItemText(3, _translate("GrisViewHeaders", "  9"))
        self.cboFile.setItemText(4, _translate("GrisViewHeaders", "  16"))
        self.cboFile.setItemText(5, _translate("GrisViewHeaders", "  25"))
        self.cboFile.setItemText(6, _translate("GrisViewHeaders", "  36"))
        self.label4_4.setText(_translate("GrisViewHeaders", "of"))
        self.lblFileNum.setText(_translate("GrisViewHeaders", "45"))
        self.btnNextFile.setText(_translate("GrisViewHeaders", "→"))
        self.btnNextFile.setShortcut(_translate("GrisViewHeaders", "Ctrl+Right"))
        self.btnLastFile.setText(_translate("GrisViewHeaders", "⇥"))
        self.btnLastFile.setShortcut(_translate("GrisViewHeaders", "Ctrl+End"))
        self.label3_3.setText(_translate("GrisViewHeaders", "Filter Keywords"))
        self.btnClearFilter.setText(_translate("GrisViewHeaders", "Clear"))
from pyqtgraph import TreeWidget
