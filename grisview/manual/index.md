## GRISView User Manual

GRISView is a visualization and analysis tool designed to work with GRIS@GREGOR calibrated datasets as distributed through [KIS Science Data Centre Arhive](https://sdc.leibniz-kis.de/).

It is intended to provide an easy, fast and interactive access to data and includes basic and advanced features to view, zoom/pan, compare, mark and measure maps and spectral regions.

GRISView is written in Python with Graphical User Interface (GUI) made using Qt cross-platform framework.


### Contents

[1. Installation](install.md)  
[2. Downloading and opening data](open_data.md)  
[3. Learning application window panels](mainwindow.md)  
[4. Fitting continuum and normalizing spectrum](spec_cont_fit.md)  
[5. Viewing headers](headers.md)  
[6. Importing inversion files](import.md)  
[7. Exporting plots](export.md)  
[8. Loading and saving sessions](sessions.md)  
