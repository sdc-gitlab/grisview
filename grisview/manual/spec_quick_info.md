## Quick info panel for spectra

Choose **Spectra > Quick Info** to show/hide the panel or press the **S** key. Here you can get quick wavelength and Stokes parameter value,
while hovering the mouse pointer over the spectral plot view box. Enable the checkbox **Hover Lines** to display vertical and horizontal tracing lines. 
