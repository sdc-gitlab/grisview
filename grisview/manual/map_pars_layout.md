## Map plot parameters selection panel

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="maps_pars_layout.jpg"/></td>        
    </tr>
</table>
 
You can select here multiple parameters, for which to display maps, and easily arrange or switch between plots. Use the numbered checkable buttons and the drop-downs to the right to add/remove maps and customize their layout. Currently, GRISView includes the following parameters for visualization:

**I, Q, U, V** - Stokes parameters as read from input FITS files  
**Q/I, U/I, V/I** - input Stokes parameters normalized to intensity      
**DoLP** - degree of linear polarization      
**DoP** - degree of polarization  
**I/Ic, Q/Ic, U/Ic, V/Ic** - Stokes parameters normalized to quiet Sun continuum intensity. To enable this set, you need to define first quiet Sun region(s) and perform continuum fitting.

In addition, imported inversion maps parameters (such as magnetic field strength and inclination) are added and can be accessed from the drop-downs here.

Use the **View** button on the right to switch between single and multiple plots view modes. In the single mode, one plot is displayed at a time, and you can cycle through multiple selected parameters by clicking left/right arrow buttons or pressing the **Page Up/Page Down** keys. In the multiple plots view all selected parameter maps are displayed simultaneously side-by-side (or in 2x2 grid).

The bottom half of the bar is visible only when time series observations are loaded. You can navigate between different maps in time series using the drop-down, the scrollbar and the **Prev**/**Next** buttons (or corresponding **Shift+Space/Space** keys). Enable the **Loop** checkbox to loop time series navigation.

To exclude a map from analysis (e.g. if it is incomplete or has bad quality) use the **Remove** button.
