## Installation

You can install GRISView from the GitLab repo directly using pip
```
pip install git+https://gitlab.leibniz-kis.de/sdc/gris/grisview.git
```

or clone the repo and run setup
```
git clone https://gitlab.leibniz-kis.de/sdc/gris/grisview.git
cd grisview
python setup.py install
```

## Usage

You can start GRISView GUI from command-line in Python environment
(that it was intalled to) as following:

```
grisview [-h] [path] [map1] [map2]

Options:
  -h          Show help message and exit
  path        Folder containing Level 1 FITS files (or single file) or program session file
  map1        Start time step number (for time series)
  map2        End time step number (for time series)
```
