## Spectral plots view area

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="spec_view_area.jpg"/></td>        
    </tr>
</table>

In this area spectral plots are displayed. Use [parameters and layouts bar](spec_pars_layout.md) 
to customize the view. If multiple plots are selected, they are arranged in vertical layout, and their view ranges are linked for easier comparison. 
Right-click plot graphics area to open the quick access context menu, which partially duplicates **Spectra** in the main menu. It also allows to autoscale a plot y range to the visible or the whole wavelength interval, or zoom plot to all data. 

Plots can be interactively scaled and panned using the mouse. Hold down the right button and
drag up/down to zoom in/out Y axis, and drag right/left to zoom in/out X axis. To pan plot, hold down the left button
and then drag. Additionally, you can zoom wavelength axis to selection. Position the mouse pointer to the start wavelength, hold **Shift** and drag. Release the button at the end wavelength to apply new view range. 

The red cursor marks current wavelength, for which [Stokes parameter maps](maps_view_area.md) are shown. Click inside the
plot view box to move the cursor or drag the cursor left/right to change current wavelength. 
Choose **Spectra > Navigation** for navigation options, and their keyboard shortcuts.  
If the cursor is out of view, use **Go to Cursor** to move current view to cursor.  
To move cursor to the current view range, use **Move Cursor to View**.  
**Link View to Cursor** fixes the cursor, when scrolling the spectra horizontally using
the slider in the [parameters and layouts bar](spec_pars_layout.md) or the left/right keyboard shortcuts.
You can show/hide grids for plots by choosing **Spectra > Grids**. 

By default, when you open a new observation, the spectrum averaged over the whole map area is displayed.
Choose **Spectra > Average Spectrum** to show/hide it or use the quick access context menu. Similarly,
once you define Quiet Sun region(s) on the maps for a given observation, you can select **Spectra > Quiet Sun Spectrum**.
**FTS Atlas** reference spectrum can be also overploted (only for **I/Ic** parameter).

Use **Spectra > Relative Wavelength Scale** for quick wavelength difference estimation. Select reference 
wavelength point and enable the option through the menu or use the *Z* key.