## Importing inversion files

In GRISView you can import higher level data sets such as inversion results that 
are available for downloading through the [Science Data Center Arhive (SDC)](https://archive.sdc.leibniz-kis.de/).
If you download a whole data set of an observation, by default it will have L2 folder containing
inversion FITS files (if there is none, perhaps no inversion was done for that particular observation). 
Otherwise, you may need to go to the archive and download L2 data separately.

Once you have the files downloaded and extracted, go to **File > Import > Inversion maps**.
Select FITS file of inversion and click **Open**. If the file type and format is correct,
you will see a message after loading is complete, showing a list of parameters that were
added for selection of map plots.
You can now select these parameters on the top panel. Inversion maps can be viewed and
analyzed same way as IQUV maps, using POI, ROI, measures, profiles and contours.

Currently, Stokes profiles fits of the spectral lines used for inversions are not
available for download through the SDC archive. Please contact SDC through the website
to get the data. If you have it, then go to 
**File > Import > Best-fit Stokes Profiles**, select a file and click **Open**.
Loading file may take some time (due to the large size). Once loaded, fitted
profiles can be checked from **POI** panel. Adding a POI will show spectral line
profile of selected pixel(s) overplotted on the observed spectra.
