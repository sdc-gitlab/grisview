## Fitting continuum and normalizing spectrum

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="spec_cont_fit.jpg"/></td>        
    </tr>
</table>

GRISView includes
spectrum continuum fitting procedure, which allows to normalize observed spectra
to the quiet Sun continuum intensity. This means that before actual fitting, a user
needs to define region(s) that do not contain any prominent surface features.
Note: in addition to the intensity map, you may want to use QUV parameter maps at
different wavelengths for comparison and quiet Sun selection.

To define a new quiet Sun region, choose **Maps > Quiet Sun Regions** (or
use context menu by right-clicking map image). In the appeared panel
on the top, click **Add Region**. You can move, resize and delete (right-click
for this option) regions, while observing changes in the resulting quiet Sun
spectrum on the right panel. It is recommended to use several regions for more
accurate definition. When finished, click **Close** on the top panel
to return to the standard map view mode.

Once Quiet Sun regions are defined, go to **Spectra > Continuum Fitting**
(or use context menu by right-clicking any spectrum plot). That will open a panel
that shows quiet Sun intensity spectrum and different options to select fitting
intervals, model and parameters. Press **+**/**-** buttons to add/remove
an interval. Clicking **Defaults** button will reset intervals to the pre-defined ones.
Press and hold interval region on the spectrum plot, then drag left/right to move
interval. You need to choose wavelength range without strong absorption/spectral
lines.

Currently, Chebyshev polynomial of the first kind is the only model for
continuum fitting. Since producing GRIS include continuum correction, it is
recommended to use default zero value for the polynomial **Degree**. For
interactive vertical spectrum adjustment, **Scaling Factor** can
be set using the edit box or the slider below. Similarly, to account for
horizontal shift when comparing with FTS atlas spectrum (separate check box to
show/hide), one can set the **Offset**. Finally, the right panel shows 
resulting polynomial coefficients for the current fit. Press **Close** button
on the top right to return to standard spectra view mode.

After fitting continuum, additional I/Ic, Q/Qc, U/Uc, V/Vc parameters will be
available for selection in the parameter lists of map and spectral plots.
