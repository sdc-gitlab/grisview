## Map plots view area

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="maps_view_area.jpg"/></td>        
    </tr>
</table>

Map plots with axes and labels are displayed here. Use [parameters selection panel](map_pars_layout.md) 
to customize the content of the area. 

Plots can be interactively scaled and panned using the mouse. Hold down the right button and
drag up/down to zoom in/out map. The same can be done using the mouse wheel. To pan map, hold down the left button
and then drag. Choose **Maps > Fit to View** or press **Ctrl+A** to fit map(s) to the current view.

The colorbars have additional controls for basic image editing. Double-click the colorbar gradient to enter
interactive mode.  In this mode, a map image histogram appears on the left. To adjust histogram range, drag up/down
top and bottom yellow markers. With this adjustments you can increase
map contrast for easier feature discrimination. Any changes made here are impermanent and will not affect the actual
data.

Histogram levels are color-mapped. GRISView includes a number of standard color schemes.
Right-click the color gradient bar to open the context menu with available color schemes. Color scheme
gradient is defined by discrete color ticks positions, and you can fully customize it. Right-click
tick triangle to open the context menu with options. You can delete a tick, set relative position
inside the view range (from 0 to 1) and change the color. To change the color, you can also double-click tick.
New ticks can be added by clicking on an empty space between ticks. Drag a tick to adjust image levels color mapping.
**Tip:** If you want to highlight a particular level interval by color, add two ticks at the interval ends and one tick
in between with custom color.

You can show/hide axes and labels by choosing **Maps > Axes and Labels** or
pressing the **A** key. Hiding axes and labels is a useful option for side-by-side maps comparison.

GRISView uses WCS header information to set **Maps > Compass** (or **N** key) north-west and disc center directions.

Choose **Maps > Grid** to show/hide the grids.

For quick access to most **Maps** menu items, position the mouse pointer over the map image area and
right-click to open the context menu.