## POI & ROI Panel

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="poi_roi_panel.jpg"/></td>        
    </tr>
</table>

Choose **Maps > POI + ROI** or press the **P** key to show/hide the panel.
Use this panel to add and control your points of interest (POI) and regions of interest (ROI). 
Enable the checkbox on the left to set POI/ROI. When undefined, a POI or ROI appears 
in the middle of the visible map area. POI position on the map is marked with a cross that can be
dragged across the image using the mouse. Similarly, by positioning the pointer over the ROI rectangle,
you can start dragging it. POI/ROI coordinates on the map are
displayed to the right of the color buttons.
For each POI or ROI the spectral graph is shown on the spectral view area using the same
color. Colors can be changed using color buttons on the panel or by double-clicking the POI/ROI on the map.
Also, right-click POI/ROI on a map to open the context menu with options.

**On/Off** checkboxes show/hide all POIs/ROIs and corresponding spectra.  
**Reset** buttons delete all POIs/ROIs and reset their colors to defaults.  
**Average Box** sets the size of the square over which the data is averaged for all POIs.  
**Hide Handles** checkbox can be useful when exporting map plots as images.
