from glob import glob
from os import path
import numpy as np
import sys


def read_lines():
    """ Read file with a pre-selected list of spectral lines."""
    list_fmt = [('wl', '<f8'), ('name', '<U8')]
    try:
        # default (run-time) path used by bundled executable from PyInstaller
        file_dir = path.join(sys._MEIPASS, 'dat')
    except AttributeError:
        file_dir = path.dirname(path.realpath(__file__))
    fn = path.join(file_dir, 'spec_lines.csv')
    res = []
    if path.exists(fn):
        res = np.genfromtxt(fn, delimiter=',', dtype=list_fmt)
        if res.size > 0:
            # filter out header/nan rows
            res = res[~np.isnan(res['wl'])]
            res.sort(order='wl')
    return res

# CSV file used here was created using in-house STOPRO program and custom input line list
# which may result in an incomplete/somewhat inaccurate list. The file
# format is following:
# wavelength (A), line name, intensity (stopro value found for a given line only)
# list_fmt = [('wl', '<f8'), ('name', '<U8'), ('intens', '<f8')]