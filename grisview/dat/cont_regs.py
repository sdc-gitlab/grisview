from astropy.units import angstrom

# Default spectral wavelength regions used for continuum fitting
default_continuum_regions = [
    (10832.52, 10833.05),
    (10836.28, 10837.59),
    (10841.13, 10842.22),
    (15650.68, 15651.65),
    (15657.01, 15657.82),
    (15635.21, 15636.02),
    (15668.08, 15669.05)
    ] * angstrom
