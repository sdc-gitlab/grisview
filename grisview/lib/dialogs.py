from PyQt6.QtCore import QDir, Qt
from PyQt6.QtGui import QPixmap, QFileSystemModel
from PyQt6.QtWidgets import QFileDialog, QLabel, QPushButton, QSpinBox, QHBoxLayout, QSpacerItem
from PyQt6.QtWidgets import QDialog, QSizePolicy, QComboBox

from grisview.gui.open_dialog import Ui_OpenDialog
from grisview.lib.misc import show_warning, show_error
from grisview.lib.grisobs import GrisObsDir

from pathlib import Path


class GrisObsOpenDialog(QDialog):
    """Dialog window class for opening GRIS observations (downloaded from
    archive.sdc.leibniz-kis.de website)."""

    def __init__(self, obs_dir='', **args):
        super().__init__()
        self.ui = Ui_OpenDialog()
        self.ui.setupUi(self)
        self.ui.tabPreviews.showEvent = self.on_preview_tab_resize
        self.ui.tabPreviews.resizeEvent = self.on_preview_tab_resize

        self.flag_open = False
        self.obsDir = None
        self.obs_dir = obs_dir
        self.obs_name = ''
        self.obs_map1 = -1
        self.obs_map2 = -1

        self.obs_previews = []
        self.id_preview = -1
        self.last_preview = -1
        self.preview_pixmap = None
        
        self._init_ui()
        if self.obs_dir:
            self.set_curr_dir(self.obs_dir)

    def _init_ui(self):
        """Prepare dialog window."""
        self.model = QFileSystemModel()
        self.model.setReadOnly(True)
        self.model.setFilter(QDir.Filter.AllDirs | QDir.Filter.Dirs | \
                             QDir.Filter.Drives | QDir.Filter.NoDotAndDotDot)
        # self.model.setRootPath(self.obs_dir)
        self.model.setRootPath("")

        tree = self.ui.treeDirView
        tree.setModel(self.model)
        tree.setCurrentIndex(self.model.index(self.obs_dir))
        for i in range(1, self.model.columnCount()):
            tree.hideColumn(i)
        tree.header().hide()
        tree.setSortingEnabled(True)
        tree.sortByColumn(0, Qt.SortOrder.AscendingOrder)

        self.keyPressEvent = self.on_key_pressed
        self.ui.treeDirView.selectionModel().selectionChanged.connect(self.on_dir_changed)
        self.ui.listObs.currentTextChanged.connect(self.on_obs_selected)
        # self.ui.editPath.setText(self.model.filePath(tree.currentIndex()))
        self.ui.editPath.returnPressed.connect(self.goto_dir)
        self.ui.btnGo.clicked.connect(self.goto_dir)
        self.ui.btnOpen.setEnabled(False)
        self.ui.btnOpen.clicked.connect(self.on_open_clicked)
        self.ui.btnRefresh.clicked.connect(self.refresh_dir)
        self.ui.panelTimeSeries.setVisible(False)
        self.ui.btnCancel.clicked.connect(self.close)
        self.ui.spbMap1.valueChanged.connect(self.update_map_start)
        self.ui.spbMap2.valueChanged.connect(self.update_map_end)
        self.ui.btnPrevImage.clicked.connect(self.display_prev_preview)
        self.ui.btnNextImage.clicked.connect(self.display_next_preview)

    def goto_dir(self):
        if Path(self.ui.editPath.text()).is_dir():
            self.ui.treeDirView.setCurrentIndex(self.model.index(self.ui.editPath.text()))
        else:
            show_error(f'Cannot open directory: {self.ui.editPath.text()}')

    def on_dir_changed(self, selected):
        """Handle folder change in the tree view."""
        try:
            self.set_curr_dir(self.model.filePath(selected.indexes()[0]))
        except:
            show_error(f'Cannot access directory: {self.ui.editPath.text()}')

    def set_curr_dir(self, obs_dir):
        """Set current working directory."""
        self.ui.editPath.setText(obs_dir) # self.model.filePath(tree.currentIndex()))
        self.obsDir = GrisObsDir(obs_dir)
        self.obs_dir = str(self.obsDir.dir_path)
        obs_list = self.obsDir.get_obs_names()
        self.ui.listObs.clear()
        if obs_list:
            self.ui.listObs.addItems(obs_list)
            self.ui.listObs.setCurrentRow(0)
        else:
            self.ui.listObs.addItem('No observations found')
            self.clear_details()
        self.obs_previews = self.obsDir.get_preview_files()
        self.ui.tabInfo.setTabText(1, f'Previews ({len(self.obs_previews)})')
        if self.obs_previews:
            self.display_preview(0)
        else:
            self.ui.lblImageName.setText('')
            self.ui.imgPreview.clear()
        flag_mult = len(self.obs_previews) > 1
        self.ui.btnPrevImage.setVisible(flag_mult)
        self.ui.btnNextImage.setVisible(flag_mult)

    def refresh_dir(self):
        self.model.setRootPath("")
        self.model.setRootPath(self.obs_dir)
        
    def on_obs_selected(self, obs_name):
        """Display selected observation information."""
        if obs_name:
            self.obs_name = obs_name
            self.obs_map1 = -1
            self.obs_map2 = -1
            num_maps = len(self.obsDir.get_obs_map_ids(obs_name))

            info = self.obsDir.get_obs_quick_info(obs_name)

            self.ui.lblName.setText(info['name'])
            self.ui.lblDate.setText(info['date'])
            self.ui.lblWaveln.setText(info['waveln'])
            self.ui.lblRes.setText(info['resolution'])
            self.ui.lblMode.setText(info['obs_mode'])
            self.ui.lblNumMaps.setText(f"{info['num_maps']}")
            self.ui.lblNumMaps.setVisible(info['num_maps'] > 1)
            self.ui.lblNumMapsL.setVisible(info['num_maps'] > 1)
            self.ui.lblNumFiles.setText(f"{info['num_files']}")
            self.ui.lblSize.setText(info['size'])
            self.ui.btnOpen.setEnabled(True)
            self.ui.panelTimeSeries.setVisible(num_maps > 1)

            if num_maps > 1:
                self.obs_map1 = 1
                self.obs_map2 = num_maps
                self.ui.spbMap1.setMinimum(1)
                self.ui.spbMap1.setMaximum(num_maps)
                self.ui.spbMap2.setMinimum(1)
                self.ui.spbMap2.setMaximum(num_maps)
                self.ui.spbMap1.setValue(1)
                self.ui.spbMap2.setValue(num_maps)

    def update_map_start(self):
        """Check and update start (and end) map index values."""
        self.obs_map1 = self.ui.spbMap1.value()
        if self.ui.spbMap1.value() > self.ui.spbMap2.value():
            self.ui.spbMap2.setValue(self.ui.spbMap1.value())

    def update_map_end(self):
        """Check and update end (and start) map index values."""
        self.obs_map2 = self.ui.spbMap2.value()
        if self.ui.spbMap1.value() > self.ui.spbMap2.value():
            self.ui.spbMap1.setValue(self.ui.spbMap2.value())

    def display_preview(self, i=-1):
        """Display preview image."""
        if i >= 0 and self.obs_previews[i] != self.last_preview:
            self.id_preview = i
            self.last_preview = self.obs_previews[i]
            self.preview_pixmap = QPixmap(str(self.obs_previews[i]))
            img_name = str(self.obs_previews[i].name)
            n_img = len(self.obs_previews)
            if n_img > 1:
                img_name += f' [{i+1}/{n_img}]'
            self.ui.lblImageName.setText(img_name)

        if self.ui.tabPreviews.isVisible():
            if self.preview_pixmap is not None:
                w = self.ui.imgPreview.width() - 10
                h = self.ui.imgPreview.height() - 10
                pix = self.preview_pixmap.scaled(w, h,
                        Qt.AspectRatioMode.KeepAspectRatio,
                        Qt.TransformationMode.SmoothTransformation)
                self.ui.imgPreview.setPixmap(pix)
                # self.ui.imgPreview.setAlignment(Qt.AlignCenter)

    def display_prev_preview(self):
        """Display previous preview file from the current folder."""
        self.display_preview(max(self.id_preview - 1, 0))

    def display_next_preview(self):
        """Display next preview file from the current folder."""
        self.display_preview(min(self.id_preview + 1, len(self.obs_previews)-1))

    def clear_details(self):
        """Clear observation details."""
        self.ui.lblName.setText('')
        self.ui.lblDate.setText('')
        self.ui.lblWaveln.setText('')
        self.ui.lblRes.setText('')
        self.ui.lblMode.setText('')
        self.ui.lblNumMaps.setText('')
        self.ui.lblNumFiles.setText('')
        self.ui.lblSize.setText('')
        self.ui.btnOpen.setEnabled(False)

    def show(self):
        self.exec()

    def on_preview_tab_resize(self, ev):
        """Update preview image, when its tab page size changes."""
        if self.obs_previews:
            self.display_preview()

    def on_key_pressed(self, ev):
        """Process key pressed event and close window if ESC is pressed."""
        if ev.key() == Qt.Key.Key_Escape:
            self.close()

    def on_open_clicked(self):
        """Process Open button click."""
        self.flag_open = True
        if self.obsDir.data_type == 'unsplit':
            info = self.obsDir.get_obs_quick_info(self.obs_name)
            if info['is_ifu_mode']:
                show_warning('GRIS/IFU unsplit data format is not supported yet')
                return
        self.close()

    def showEvent(self, ev):
        self.flag_open = False

    def closeEvent(self, ev):        
        if not self.flag_open:
            self.obs_dir = ''
            self.obs_name = ''


class ImageSaveDialog(QFileDialog):
    """Save file dialog window to export program map/spectrum panel as an image
    with custom options for output width and height."""

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle('Save As Image')
        self.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
        self.setNameFilter("PNG Image (*.png);;"
                           "JPG Image (*.jpg);;"
                           "TIFF Image (*.tiff);;"
                           "BMP Image (*.bmp)")
        lbl_opt = QLabel()
        lbl_opt.setText("Options:")
        lbl_width = QLabel()
        lbl_width.setText("Width")
        lbl_height = QLabel()
        lbl_height.setText("Height")
        hspacer = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)
        self.spbWidth = QSpinBox()
        self.spbHeight = QSpinBox()
        self.spbWidth.setAccelerated(True)
        self.spbHeight.setAccelerated(True)
        self.spbWidth.setFixedWidth(75)
        self.spbHeight.setFixedWidth(75)
        self.spbWidth.setMaximum(5000)
        self.spbHeight.setMaximum(5000)
        self.btnReset = QPushButton()
        self.btnReset.setText('Reset')
        self.cboUnits = QComboBox()
        self.cboUnits.addItems(['percents', 'pixels'])
        self.cboUnits.currentTextChanged.connect(self._update_units)
        self.spbWidth.valueChanged.connect(self._update_width)
        self.spbHeight.valueChanged.connect(self._update_height)
        self.btnReset.clicked.connect(self._reset_size)

        self.width_orig = 0
        self.height_orig = 0
        self.width = 0
        self.height = 0

        self.units = 'percents'

        hlayout = QHBoxLayout()
        hlayout.addWidget(lbl_width)
        hlayout.addWidget(self.spbWidth)
        hlayout.addWidget(lbl_height)
        hlayout.addWidget(self.spbHeight)
        hlayout.addWidget(self.cboUnits)
        hlayout.addWidget(self.btnReset)
        hlayout.addItem(hspacer)

        self.layout().addWidget(lbl_opt, 4, 0)
        self.layout().addLayout(hlayout, 4, 1)

        # opt = self.Options()
        # opt |= QFileDialog.DontUseNativeDialog

    def set_image_size(self, w, h):
        """Set output image width and height."""
        self.spbWidth.blockSignals(True)
        self.spbHeight.blockSignals(True)
        if self.units == 'pixels':
            self.spbWidth.setValue(w)
            self.spbHeight.setValue(h)
        if self.units == 'percents':
            self.spbWidth.setValue(100)
            self.spbHeight.setValue(100)
        self.width = w
        self.height = h
        self.width_orig = w
        self.height_orig = h
        self.spbWidth.blockSignals(False)
        self.spbHeight.blockSignals(False)

    def image_width(self):
        """Get output image width."""
        return self.width

    def image_format(self):
        """Get output image format."""
        filt = self.selectedNameFilter()
        return filt.split('(')[1][2:-1]

    def image_filename(self):
        """Get output image file name."""
        res = self.selectedFiles()[0]
        if not res.endswith(self.image_format()):
            res += '.' + self.image_format()
        return res

    def _reset_size(self):
        """Reset output image size to the panel current screen size."""
        if self.units == 'pixels':
            self.spbWidth.setValue(self.width_orig)
        if self.units == 'percents':
            self.spbWidth.setValue(100)

    def _update_width(self, val):
        """Update output image size after width change."""
        if self.units == 'pixels':
            self.width = val
        if self.units == 'percents':
            self.width = val * self.width_orig // 100
        self.height = self.width * self.height_orig // self.width_orig
        self.spbHeight.blockSignals(True)
        if self.units == 'pixels':
            self.spbHeight.setValue(self.height)
        if self.units == 'percents':
            self.spbHeight.setValue(val)
        self.spbHeight.blockSignals(False)

    def _update_height(self, val):
        """Update output image size after height change."""
        if self.units == 'pixels':
            self.height = val
        if self.units == 'percents':
            self.height = val * self.height_orig // 100
        self.width = self.height * self.width_orig // self.height_orig
        self.spbWidth.blockSignals(True)
        if self.units == 'pixels':
            self.spbWidth.setValue(self.width)
        if self.units == 'percents':
            self.spbWidth.setValue(val)
        self.spbWidth.blockSignals(False)

    def _update_units(self, text):
        """Update width/height after unit change (pixel/percents)."""
        self.units = text
        if self.units == 'pixels':
            self.spbWidth.setValue(self.width)
        if self.units == 'percents':
            self.spbWidth.setValue(self.width * 100 // self.width_orig)
