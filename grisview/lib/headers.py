from PyQt6.QtWidgets import QDialog, QHeaderView, QTreeWidgetItem
from pyqtgraph.parametertree.parameterTypes import GroupParameterItem, TextParameterItem
from pyqtgraph.parametertree import Parameter

from grisview.gui.headers_form import Ui_GrisViewHeaders
from os import path


class GrisViewHeaders(QDialog):
    """Class for displaying loaded observation files headers."""

    def __init__(self, obs):
        super().__init__()
        self.ui = Ui_GrisViewHeaders()
        self.ui.setupUi(self)
        self.obs = obs
        self.id_map = -1
        self.id_pos = -1
        self.id_file = -1
        self.filter_key = ''
        self._prepare()

    def _prepare(self):
        """Prepare dialog window."""
        self.setWindowTitle('View Headers - ' + self.obs.name)
        if self.obs.num_maps > 1:
            cb = self.ui.cboMap
            cb.blockSignals(True)
            cb.clear()
            cb.addItems([f'{i + 1}' for i in range(self.obs.num_maps)])
            cb.setCurrentIndex(0)
            cb.blockSignals(False)
            self.ui.lblMapNum.setText(str(self.obs.num_maps))
        self.ui.lblPath.setText(path.dirname(self.obs.maps[0].files[0]))
        
        t = self.ui.wgtHeader
        t.setColumnCount(3)
        t.setHeaderLabels(['Keyword', 'Value', 'Comment'])
        t.setAlternatingRowColors(True)
        t.setAnimated(False)
        t.setAcceptDrops(False)
        t.setDragEnabled(False)
        # t.header().setResizeMode(QHeaderView.ResizeMode.ResizeToContents)
        t.header().setSectionResizeMode(QHeaderView.ResizeMode.ResizeToContents)
        t.header().setStretchLastSection(False)
        self.header_sections = []

        self.ui.panelMaps.setVisible(self.obs.data_type == 'split' and self.obs.num_maps > 1)
        self.ui.panelSlits.setVisible(self.obs.data_type == 'split' and not self.obs.is_ifu_mode)
        self.ui.panelFiles.setVisible(self.obs.data_type == 'unsplit')
        if self.obs.data_type == 'split':
            self.ui.cboMap.currentIndexChanged.connect(self.on_map_id_change)
            self.ui.cboSlit.currentIndexChanged.connect(self.on_pos_id_change)
            self.ui.btnFirstMap.clicked.connect(self.go_first_map)
            self.ui.btnPrevMap.clicked.connect(self.go_prev_map)
            self.ui.btnNextMap.clicked.connect(self.go_next_map)
            self.ui.btnLastMap.clicked.connect(self.go_last_map)
            self.ui.btnFirstSlit.clicked.connect(self.go_first_slit)
            self.ui.btnPrevSlit.clicked.connect(self.go_prev_slit)
            self.ui.btnNextSlit.clicked.connect(self.go_next_slit)
            self.ui.btnLastSlit.clicked.connect(self.go_last_slit)
            self.on_map_id_change(0)
        elif self.obs.data_type == 'unsplit':
            self.ui.cboFile.currentIndexChanged.connect(self.on_file_id_change)
            self.ui.btnFirstFile.clicked.connect(self.go_first_file)
            self.ui.btnPrevFile.clicked.connect(self.go_prev_file)
            self.ui.btnNextFile.clicked.connect(self.go_next_file)
            self.ui.btnLastFile.clicked.connect(self.go_last_file)
            self.ui.lblFileNum.setText(str(len(self.obs.files)))
            cb = self.ui.cboFile
            cb.blockSignals(True)
            cb.clear()
            cb.addItems([f'{i + 1}' for i in range(len(self.obs.files))])
            cb.setMaxVisibleItems(25)            
            cb.blockSignals(False)            
            self.on_file_id_change(0)
        self.ui.editFilterKeywords.textChanged.connect(self.update_filter)
        self.ui.btnClearFilter.clicked.connect(self.ui.editFilterKeywords.clear)
        
    def show(self):
        self.exec()

    def on_map_id_change(self, i):
        """Update window after current map index change."""
        self.id_map = i
        cb = self.ui.cboSlit
        cb.blockSignals(True)
        cb.clear()
        cb.addItems([f'{i + 1}' for i in range(self.obs.maps[i].num_files)])
        cb.setMaxVisibleItems(25)
        cb.setCurrentIndex(0)
        cb.blockSignals(False)
        self.ui.lblSlitNum.setText(str(self.obs.maps[i].num_files))
        self.on_pos_id_change(0)

    def go_first_map(self):
        """Go to first map (for time series only)."""
        self.ui.cboMap.setCurrentIndex(0)

    def go_prev_map(self):
        """Go to previous map (for time series only)."""
        if self.id_map > 0:
            self.ui.cboMap.setCurrentIndex(self.id_map - 1)

    def go_next_map(self):
        """Go to next map (for time series only)."""
        if self.id_map < self.ui.cboMap.count() - 1:
            self.ui.cboMap.setCurrentIndex(self.id_map + 1)

    def go_last_map(self):
        """Go to last map (for time series only)."""
        self.ui.cboMap.setCurrentIndex(self.ui.cboMap.count() - 1)

    def go_first_slit(self):
        """Go to first slit position file of the current map."""
        self.ui.cboSlit.setCurrentIndex(0)

    def go_prev_slit(self):
        """Go to previous slit position file of the current map."""
        if self.id_pos > 0:
            self.ui.cboSlit.setCurrentIndex(self.id_pos - 1)

    def go_next_slit(self):
        """Go to next slit position file of the current map."""        
        if self.id_pos < self.ui.cboSlit.count() - 1:
            self.ui.cboSlit.setCurrentIndex(self.id_pos + 1)

    def go_last_slit(self):
        """Go to previous slit position file of the current map."""
        self.ui.cboSlit.setCurrentIndex(self.ui.cboSlit.count() - 1)

    def on_pos_id_change(self, i):
        """Go to selected slit position file of the current map."""
        self.id_pos = i
        self.header = self.obs.maps[self.id_map].headers[i]
        self.ui.lblFile.setText(path.basename(self.obs.maps[self.id_map].files[i]))
        self.display_header()

    def go_first_file(self):
        """Go to first slit position file of the current map."""
        self.ui.cboFile.setCurrentIndex(0)

    def go_prev_file(self):
        """Go to previous slit position file of the current map."""
        if self.id_file > 0:
            self.ui.cboFile.setCurrentIndex(self.id_file - 1)

    def go_next_file(self):
        """Go to next slit position file of the current map."""        
        if self.id_file < self.ui.cboFile.count() - 1:
            self.ui.cboFile.setCurrentIndex(self.id_file + 1)

    def go_last_file(self):
        """Go to previous slit position file of the current map."""
        self.ui.cboFile.setCurrentIndex(self.ui.cboFile.count() - 1)
        
    def on_file_id_change(self, i):
        """Update window after current map index change."""
        self.id_file = i
        self.header = self.obs.headers[i]
        self.ui.lblFile.setText(path.basename(self.obs.files[i]))
        self.display_header()

    def display_header(self):
        """Display FITS header inside window tree widget."""
        sect_state = self.get_sect_state()
        hdr = self.header
        t = self.ui.wgtHeader
        t.clear()
        values = []
        for card in hdr.cards:
            try:
                values.append(str(card.value))
            except:
                values.append('UNDEFINED')        
        self.header_sections = []
        sect = self.header_sections
        if not self.filter_key:
            if self.obs.data_type != 'unsplit':
                sect.append(GroupParameterItem(Parameter.create(name='Basic'), 0))
            else:
                sect.append(GroupParameterItem(Parameter.create(name='Header'), 0))
        else:
            sect.append(GroupParameterItem(Parameter.create(name='Matching keywords'), 0))
        t.addTopLevelItem(sect[-1])
        for i, card in enumerate(hdr.cards):
            if self.filter_key:
                if self.filter_key not in card.keyword:
                    continue
            if card.keyword == 'COMMENT':
                if len(set(values[i])) == 1:
                    continue
                if values[i][:3] == '---':
                    sect.append(GroupParameterItem(
                        Parameter.create(name=values[i].strip('-')), 0))
                    t.addTopLevelItem(sect[-1])
                else:
                    sect[-1].addChild(QTreeWidgetItem([card.keyword, values[i], card.comment]))
            else:
                sect[-1].addChild(QTreeWidgetItem([card.keyword, values[i], card.comment]))
        if self.filter_key:
            sect[0].param.setName(f'Matching keywords ({sect[0].childCount()})')
        if sect_state:
            self.set_sect_state(sect_state)
        else:
            for s in self.header_sections:
                s.setExpanded(self.obs.data_type == 'unsplit')
            if self.obs.data_type == 'split':
                self.header_sections[2].setExpanded(True)

    def get_sect_state(self):
        """Save collapsed/expanded tree state."""
        sect_state = {}
        for s in self.header_sections:
            sect_state[s.param.title()] = s.isExpanded()
        return sect_state

    def set_sect_state(self, state):
        """Restore collapsed/expanded tree state."""
        if state:
            for s in self.header_sections:
                if s.param.title() in state:
                    s.setExpanded(state[s.param.title()])

    def update_filter(self, txt):
        """Update header keyword filter."""
        self.filter_key = txt.upper()
        self.display_header()
