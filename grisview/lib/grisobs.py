import warnings

from PyQt6.QtCore import QThread, pyqtSignal, QObject, QEventLoop, Qt

import os
from os import path
import regex as re
from pathlib import Path
from datetime import datetime

import numpy as np
import astropy.io.fits as fitsio
from PyQt6.QtWidgets import QProgressDialog
from astropy.wcs import WCS
from astropy.wcs.utils import proj_plane_pixel_scales
from astropy.utils.exceptions import AstropyWarning
import astropy.units as u

from grisview.lib.misc import show_warning, show_error, \
    show_question_yes, show_info, format_size_bytes
from grisview.lib.plotpars import PlotParam

datafile_patterns_quick = {
    'split': 'gris*.fits', 
    'unsplit': '[0-9][0-9][a-z][a-z][a-z][0-9][0-9].[0-9][0-9][0-9]*[cr]'}

datafile_patterns_re = {
    'split': re.compile(r'(gris).*_(\d{8})[T_]\d+(?:_[a-z\d]+)?_(\d{3})(?:_[a-z\d]+)?_(\d{3})(?:_\d+.*)?.fits$'),
    'unsplit': re.compile(r'(\d{2}[a-z]{3}\d{2}).(\d{3})(?:-\d{2})?(?:cc|r)?$')}

preview_patterns_re = re.compile('(gris.*)_(\d{8})_(\d+)_.*.png$')



class GrisObsDir:
    """Class to scan folders for GRIS observations."""

    def __init__(self, obs_path):
        self.dir_path = Path(obs_path).resolve()
        self.data_type = ''
        self.previews = []
        self._scan_dir()
        
    def _scan_dir(self):
        """Scan folder and return a list of GRIS FITS files."""
        self.files = []
        for typ, mask in datafile_patterns_quick.items():
            all_files = [fn for fn in self.dir_path.glob(mask)]
            if all_files:
                self.data_type = typ
                break
        if all_files:
            re_mask = datafile_patterns_re[self.data_type]
            self.files = sorted([str(fn) for fn in all_files if re_mask.match(fn.name)])

    def has_obs(self, obs_name=''):
        """Check if folder contains files of any or particular observation."""
        if obs_name:
            return len(self.get_obs_files(obs_name)) > 0
        else:
            return len(self.files) > 0

    def has_obs_map(self, obs_name, map_id):
        """Check if an observation has a map with a given index."""
        return map_id in self.get_obs_map_ids(obs_name)
    
    def get_file_obs_name(self, fn):
        """Get observation name from FITS file name"""
        if self.data_type:
            re_mask = datafile_patterns_re[self.data_type]
            info = re_mask.match(Path(fn).name).groups()
            if self.data_type == 'split':
                return '_'.join(info[:-1])
            elif self.data_type == 'unsplit':
                date_raw = datetime.strptime(info[0], '%d%b%y')
                date_str = datetime.strftime(date_raw, '%Y%m%d')
                return f'gris_{date_str}_{info[1]}'
        else:
            return ''

    @staticmethod
    def get_preview_obs_name(fn):
        """Get observation name from preview image file name."""
        info = preview_patterns_re.match(Path(fn).name).groups()
        return '_'.join(info)
    
    def get_file_obs_map_ids(self, fn):
        """Get observation map index from input file name."""
        if self.data_type:
            re_mask = datafile_patterns_re[self.data_type]
            info = re_mask.match(Path(fn).name).groups()
            if self.data_type == 'split':
                return int(info[-1])
            elif self.data_type == 'unsplit':
                with fitsio.open(fn, memmap=False) as fits:
                    hdr = fits[0].header
                try:
                    id1 = hdr['ISERIE']
                    id2 = hdr['ISERIE', -1]
                    if id1 != id2:
                        return [id1, id2]
                    else:
                        return id1
                except:
                    return -1
        else:
            return -1
                
    def get_obs_names(self):
        """Get names of all observations found in the given folder."""
        return sorted(set([self.get_file_obs_name(fn) for fn in self.files]))

    def get_obs_map_ids(self, obs_name):
        """Get all map indices for a given observation name."""
        obs_files = self.get_obs_files(obs_name)
        if self.data_type == 'split':            
            return sorted(set([self.get_file_obs_map_ids(fn) for fn in obs_files]))
        elif self.data_type == 'unsplit':
            ids = []
            for fn in obs_files:
                id_file = self.get_file_obs_map_ids(fn)
                if isinstance(id_file, list):
                    ids.extend([i for i in range(id_file[0], id_file[1]+1)])
                else:
                    ids.append(id_file)
            ids = sorted(set(ids))
            if -1 in ids:
                ids.remove(-1)
            return ids

    def get_obs_files(self, obs_name, obs_map_id=-1):
        """Get selected observation files.

        Args:
            obs_name (str): observation name
            obs_map_id (int, optional): observation time series map index. Defaults to -1.

        Returns:
            list: list of observation files
        """
        if obs_map_id < 0:
            return [fn for fn in self.files
                    if self.get_file_obs_name(fn) == obs_name]
        else:
            if self.data_type == 'split':
                return [fn for fn in self.files
                        if self.get_file_obs_name(fn) == obs_name and
                        self.get_file_obs_map_ids(fn) == obs_map_id]
            elif self.data_type == 'unsplit':
                obs_files = []
                for fn in self.files:
                    if self.get_file_obs_name(fn) == obs_name:
                        map_id = self.get_file_obs_map_ids(fn)
                        if isinstance(map_id, list):
                            if map_id[0] <= obs_map_id <= map_id[1]:
                                obs_files.append(fn)
                            if map_id[0] > obs_map_id:
                                break
                        else:
                            if (map_id > 0 and map_id == obs_map_id) or \
                               (map_id < 0 and obs_files):
                                obs_files.append(fn)
                            if map_id > obs_map_id:
                                break
                return obs_files
                
    def get_preview_files(self):
        """Scan folder and return list of GRIS observation preview images."""
        previews = sorted(self.dir_path.glob('gris*.png'))
        previews += sorted(self.dir_path.glob('gris*.gif'))
        return previews

    def get_obs_quick_info(self, obs_name):
        """Retrieve information about an observation."""
        res = dict(name=obs_name,
                   date='unknown',
                   size='unknown',
                   obs_mode='unknown',
                   waveln='unknown',
                   num_x=-1,
                   num_y=-1,
                   resolution='unknown',
                   is_ifu_mode=None)
        obs_files = self.get_obs_files(obs_name)
        obs_map_ids = self.get_obs_map_ids(obs_name)
        res['num_maps'] = len(obs_map_ids)
        res['num_files'] = len(obs_files)
        try:
            sizeb = np.sum([Path(fn).stat().st_size for fn in obs_files])
            res['size'] = format_size_bytes(sizeb)
        except:
            pass
        try:
            with fitsio.open(obs_files[0], memmap=False) as fits:
                hdr = fits[0].header
            if self.data_type == 'split':
                res['obs_mode'] = hdr['OBS_MODE']
                res['waveln'] = f"{hdr['AWAVLNTH']:.0f}"
                res['date'] = hdr['DATE-BEG'].replace('T', ' ').rstrip('0')
                res['is_ifu_mode'] = 'ifu' in obs_name
                if not res['is_ifu_mode']:
                    res['num_x'] = hdr['NSTEPS']
                else:
                    res['num_x'] = hdr['NAXIS1']
                res['num_y'] = hdr['NAXIS2']
            elif self.data_type == 'unsplit':
                res['waveln'] = f"{hdr['WAVELENG'] * 10:.0f}"
                res['date'] = hdr['DATE-OBS'] + ' ' + hdr['UTIME']
                res['is_ifu_mode'] = (hdr['IMGSYS'] == 'IFU')
                res['num_x'] = hdr['STEPS']
                res['num_y'] = hdr['NAXIS2']
            res['resolution'] = f"{res['num_x']}x{res['num_y']}"
        except:
            pass
        return res


class FitsFilesReader(QThread):
    """
    Class that reads a list of FITS files in a separate thread and yield files
    header and data.

    Attributes:
        files (list): list of filenames
        sig_file_loaded (pyqtSignal): emitted for each FITS file loaded
        sig_file_failed (pyqtSignal): emits filename, which could not be read
    """
    sig_file_loaded = pyqtSignal(int, fitsio.Header, np.ndarray)
    sig_file_failed = pyqtSignal(str)
    sig_file_aborted = pyqtSignal()

    def __init__(self, files=None):
        super().__init__()
        self.files = files

    def run(self):
        for i, fn in enumerate(self.files):
            try:
                with fitsio.open(fn, memmap=False) as fits:
                    self.sig_file_loaded.emit(i, fits[0].header, fits[0].data)
                # added for easier thread interruption
                self.msleep(2)
            except:
                self.sig_file_failed.emit(fn)
                break
            if self.isInterruptionRequested():
                self.sig_file_aborted.emit()
                break


class GrisMap(QObject):
    """
    Class to manage single GRIS map (as part of time series) including FITS file
    headers and data for all instrument slit positions.

    Attributes:
        files (list): list of file names
        file_loaded (pyqtSignal): emitted for each FITS file loaded
        num_x (int), num_y (int): width (number of slit positions) and height (coordinate along the slit)
        data (ndarray): 4D data cube in a shape of (num_stokes, num_waveln, num_y, num_x)
        headers (dict): dictionary with filenames as keys and file headers as values
    """
    sig_file_loaded = pyqtSignal()
    sig_file_failed = pyqtSignal(str)
    sig_file_aborted = pyqtSignal()

    def __init__(self, files=None):
        super().__init__()
        self.num_x = 0
        self.num_y = 0
        self.num_waveln = 0
        self.num_stokes = 0
        self.data = []
        # self.data_spec_avg = []
        self.headers = []
        self.wcs_coord = []
        self.timestamps = []
        self.files = files
        self.num_files = len(self.files)
        self.is_ifu_mode = False
        self.thread_read = FitsFilesReader(self.files)
        self.aborted_read = False

    def reset(self):
        """Reset to empty map."""
        self.__dict__.update(dict(
            files=[], num_files=0,
            num_x=0, num_y=0,
            num_waveln=0, num_stokes=0,
            data=[], headers=[]
        ))

    def generate_random(self, w=449, h=400):
        """Generate random map."""
        self.num_x = w
        self.num_y = h
        self.num_stokes = 4
        self.num_waveln = 1
        self.data = np.random.rand(self.num_stokes, self.num_waveln, self.num_y, self.num_x)

    def read_files(self):
        """Read map data from files, account for different instrument modes."""
        try:
            with fitsio.open(self.files[0], memmap=False) as fits:
                data_shape = fits[0].data.shape
        except:
            self._process_file_failed(self.files[0])
            return
        self.is_ifu_mode = '-ifu' in path.basename(self.files[0])
        if len(data_shape) == 4:
            if self.is_ifu_mode:
                self.num_x = data_shape[3]
            else:
                self.num_x = self.num_files
            self.num_y = data_shape[2]
            self.num_stokes = data_shape[0]
            self.num_waveln = data_shape[1]
        if len(data_shape) == 3:
            self.num_x = self.num_files
            self.num_y = data_shape[1]
            self.num_stokes = 1
            self.num_waveln = data_shape[0]
        self.wcs_coord = [None] * self.num_files
        self.timestamps = [None] * self.num_files
                        
        self.data = np.ndarray((self.num_stokes, self.num_waveln, self.num_y, self.num_x))
        self.headers = [None] * self.num_files        

        self.thread_read.sig_file_loaded.connect(self._process_file_loaded)
        self.thread_read.sig_file_failed.connect(self._process_file_failed)
        self.thread_read.sig_file_aborted.connect(self._process_file_aborted)
        self.thread_read.start()

    def abort_read_files(self):
        """Abort file reading thread."""
        self.aborted_read = True
        self.thread_read.requestInterruption()

    def _process_file_loaded(self, i, hdr, dat):
        """Process loaded file header and data.

        Args:
            i (int): file index (slit position)
            hdr (dict): fits file header
            dat (ndarray): filts file data
        """
        if not self.aborted_read:
            self.headers[i] = hdr
            if self.is_ifu_mode:
                self.data = dat.astype(np.float32)
            else:
                if self.num_stokes == 1:
                    self.data[:, :, :, i] = dat[:, :, 0].astype(np.float32)
                else:
                    self.data[:, :, :, i] = dat[:, :, :, 0].astype(np.float32)
            self.timestamps[i] = hdr['DATE-BEG']
            warnings.simplefilter('ignore', category=AstropyWarning)
            self.wcs_coord[i] = WCS(hdr).sub(2)
            warnings.simplefilter('default', category=AstropyWarning)
            self.sig_file_loaded.emit()

    def _process_file_failed(self, fn):
        """Run if file could not be loaded, reset map."""
        self.reset()
        self.sig_file_failed.emit(fn)

    def _process_file_aborted(self):
        """Run if files loading was aborted by user, reset map."""
        self.reset()
        self.sig_file_aborted.emit()



class GrisMapUnsplit(GrisMap):
    
    def __init__(self, files, iseries):
        super().__init__(files)        
        self.iseries = iseries
        self.num_series = -1
        
    def read_files(self):
        """Read map data from files, account for different instrument modes."""
        try:
            with fitsio.open(self.files[0], memmap=False) as fits:
                hdr = fits[0].header
                self.num_series = hdr['SERIES']
                self.num_y = hdr['NAXIS2']
                self.num_stokes = hdr['STATES']
                self.num_waveln = hdr['NAXIS1']
        except:
            self._process_file_failed(self.files[0])
            return
        self.num_x = 0
        for fn in self.files:
            with fitsio.open(fn, memmap=False) as fits:
                hdr = fits[0].header
                num_x_file = int(fits[0].data.shape[0] / self.num_stokes)
                if self.num_series == 1 or 'ISERIE' not in hdr:
                    self.num_x += num_x_file
                else:
                    try:                        
                        istep = (self.iseries - hdr['ISERIE']) * hdr['STEPS']
                        while self.num_x < hdr['ISTEP', istep]:
                            self.num_x += 1
                            istep += 1
                    except:
                        if istep > num_x_file:
                            self.num_x -= 1
                        continue
        
        self.wcs_coord = None
        self.timestamps = [None] * self.num_x
                        
        self.data = np.ndarray((self.num_stokes, self.num_waveln, self.num_y, self.num_x))
        self.headers = [None] * self.num_files        

        self.thread_read.sig_file_loaded.connect(self._process_file_loaded)
        self.thread_read.sig_file_failed.connect(self._process_file_failed)
        self.thread_read.sig_file_aborted.connect(self._process_file_aborted)
        self.thread_read.start()
    
    def _process_file_loaded(self, i, hdr, dat):
        """Process loaded file header and data.

        Args:
            i (int): file index (slit position)
            hdr (dict): fits file header
            dat (ndarray): filts file data
        """
        if not self.aborted_read:
            self.headers[i] = hdr
            dat = np.swapaxes(dat, 0, 2)
            if self.num_series == 1 or 'ISERIE' not in hdr:
                istep0 = hdr['ISTEP'] - 1
                istep_off = 0
                num_steps = int(dat.shape[2] / self.num_stokes)
            else:
                istep0 = 0
                istep_off = (self.iseries - hdr['ISERIE']) * hdr['STEPS']
                num_steps = self.num_x
                num_steps_max = int(dat.shape[2] / self.num_stokes)
                if istep_off + num_steps > num_steps_max:
                    num_steps = num_steps_max % self.num_x                
            if self.num_stokes == 1:
                self.data[:, :, :, istep0:istep0+num_steps] = dat.astype(np.float32)
            else:
                for p in range(self.num_stokes):
                    self.data[p, :, :, istep0:istep0+num_steps] = \
                        dat[:, :, 
                            istep_off*self.num_stokes + p:
                            (istep_off + num_steps)*self.num_stokes:
                            self.num_stokes].astype(np.float32)
            for istep in range(istep0, istep0+num_steps):
                self.timestamps[istep] = hdr['UT', istep + istep_off - istep0]
            self.sig_file_loaded.emit()
    
    
    
class GrisObs(QObject):
    """Class for reading and describing GRIS observations, including
    multi-map time series."""

    sig_failed = pyqtSignal()
    sig_aborted = pyqtSignal()
    sig_loaded = pyqtSignal()

    def __init__(self, obs_dir='', obs_name='', map1=-1, map2=-1):
        """Init observation instance.

        Args:
            obs_dir (str, optional): folder containing Level 1 FITS files. Defaults to ''.
            obs_name (str, optional): name of the observation to load. Defaults to '' (all).
            map1 (int, optional): index of the first map to load (for time series). Defaults to -1.
            map2 (int, optional): index of the last map to load (for time series). Defaults to -1.
        """
        super().__init__()

        self.progress = QProgressDialog("Loading observation...",
                                        "Abort", 0, 1)
        self.progress.setWindowModality(Qt.WindowModality.WindowModal)
        self.progress.setWindowFlag(Qt.WindowType.FramelessWindowHint)
        self.progress.canceled.connect(self.abort_read_maps)
        self.progress.setLabelText('Scanning directory...')
        self.progress.show()

        self.obsDir = GrisObsDir(obs_dir)

        if not obs_name:
            obs_name = self.obsDir.get_obs_names()[0]
        self.__dict__.update(dict(
            dir_path=str(self.obsDir.dir_path),
            data_type=self.obsDir.data_type,
            name=obs_name,
            num_files=0,
            maps=[],
            map_ids=[],
            num_maps=0,
            map_start=map1,
            map_end=map2,
            num_stokes=0,
            num_x=0,
            num_y=0,
            num_waveln=0,
            scale_x=0.0,
            scale_y=0.0,
        ))

        self.progress.setLabelText('Getting map files...')
        self.orig_map_ids = self.obsDir.get_obs_map_ids(self.name)
        if 0 < self.map_start <= len(self.orig_map_ids):
            if self.map_end < 0:
                self.orig_map_ids = [self.orig_map_ids[self.map_start - 1]]
            elif self.map_end <= len(self.orig_map_ids):
                self.orig_map_ids = self.orig_map_ids[self.map_start - 1:self.map_end]

        self.files = self.obsDir.get_obs_files(self.name)
        self.headers = []
        if self.data_type == 'split':
            for id in self.orig_map_ids:
                self.maps.append(GrisMap(self.obsDir.get_obs_files(self.name, id)))
        elif self.data_type == 'unsplit':
            for id in self.orig_map_ids:
                self.maps.append(GrisMapUnsplit(self.obsDir.get_obs_files(self.name, id), id))
        self.num_maps = len(self.orig_map_ids)
        self.num_files = sum([m.num_files for m in self.maps])
        self.progress.setLabelText('Preparing...')
        self.is_time_series = self.num_maps > 1
        self.is_ifu_mode = 'ifu' in self.name

        self._is_load_aborted = False

    def check_time_series(self):
        """ Check if time series is complete or missing maps/files."""
        # in complete series number of files per map is the same
        num_diff_num = len(set([m.num_files for m in self.maps]))
        if num_diff_num > 2:
            # at least 3 maps have different number of slit scans
            show_error('Observation time series is missing files\n'
                       'Please check and try to re-download if necessary')
            return False
        incomplete_series = num_diff_num == 2 and \
                            self.maps[-1].num_files != self.maps[-2].num_files
        if incomplete_series:
            return show_question_yes('Observation time series is not complete.\n'
                                     'Do you want to continue?')
        else:
            return num_diff_num == 1

    def read_maps(self):
        """Read observation maps in a separate thread."""
        if self.check_time_series():
            loop = QEventLoop()
            self.num_files_loaded = 0
            self.progress.setMinimum(0)
            self.progress.setMaximum(self.num_files)
            for m in self.maps:
                if self._is_load_aborted:
                    loop.quit()
                    break
                m.sig_file_loaded.connect(self.get_read_progress)
                m.sig_file_failed.connect(self._process_file_failed)
                m.sig_file_aborted.connect(self._process_file_aborted)
                m.thread_read.finished.connect(loop.quit)
                m.read_files()
                loop.exec()
        else:
            self.abort_read_maps()

    def abort_read_maps(self):
        """Abort file reading thread."""
        self.progress.hide()
        for obs_map in self.maps:
            if obs_map.thread_read.isRunning():
                obs_map.abort_read_files()
        self._is_load_aborted = True
        self.sig_aborted.emit()        

    def get_read_progress(self):
        """Show loading progress and emit signal when finished."""
        if not self._is_load_aborted:
            self.num_files_loaded += 1
            self.progress.setLabelText(f'Loading: {self.num_files_loaded} files read...')
            self.progress.setValue(self.num_files_loaded)
            # self.sig_file_loaded.emit(self.num_files_loaded)
            if self.num_files_loaded == self.num_files:
                self._get_params()
                self.sig_loaded.emit()

    def _process_file_failed(self, fn):
        """Run if file could not be loaded."""
        self.maps = []
        self.sig_failed.emit()
        show_error('Could not read FITS file:\n' + fn)

    def _process_file_aborted(self):
        """Run if files loading was aborted by user."""
        self.maps = []
        show_warning('Loading files has been aborted.')
        # self.file_aborted.emit()

    def _get_params(self):
        """Retrieve observation parameters."""
        map_ref = self.maps[0]
        hdr_ref = map_ref.headers[0]
        self.num_stokes = map_ref.num_stokes
        self.num_waveln = map_ref.num_waveln
        self.num_x = map_ref.num_x
        self.num_y = map_ref.num_y
        if self.data_type == 'split':
            warnings.simplefilter('ignore', category=AstropyWarning)
            wcs = WCS(hdr_ref)
            self.wcs_spec = wcs.sub([3])
            warnings.simplefilter('default', category=AstropyWarning)
            self.scale_x, self.scale_y = proj_plane_pixel_scales(map_ref.wcs_coord[0])
            self.scale_x, self.scale_y = ((self.scale_x, self.scale_y) * u.deg).to(u.arcsec).value
        elif self.data_type == 'unsplit':
            funiq = []
            for f in self.files:
                for m in self.maps:
                    for i, fm in enumerate(m.files):
                        if fm == f and fm not in funiq:
                            self.headers.append(m.headers[i])
                            funiq.append(fm)
            self.wcs_spec = WCS(naxis=1)
            wl_wcs =self.wcs_spec.wcs            
            wl_wcs.crpix = [1.0]
            wl_wcs.ctype = ['WAVE']
            wl_wcs.cunit = ['m']
            try:
                wl_wcs.crval = [(hdr_ref["FF1WLOFF"] + hdr_ref["FF2WLOFF"]) * 1e-10 / 2]
                wl_wcs.cdelt = [(hdr_ref["FF1WLDSP"] + hdr_ref["FF2WLDSP"]) * 1e-10 / 2]
            except:
                try:
                    wl_wcs.crval = [hdr_ref["FF1WLOFF"] * 1e-10]
                    wl_wcs.cdelt = [hdr_ref["FF1WLDSP"] * 1e-10]
                except:
                    self.wcs_spec = None
            self.scale_x, self.scale_y = 0.135, 0.135
            
    def delete_map(self, map_id):
        """Delete observation map by its index."""
        try:
            if 0 <= map_id < self.num_maps:
                self.maps.pop(map_id)
                self.orig_map_ids.pop(map_id)
                self.num_maps -= 1
            return True
        except:
            return False

    def coord_px_to_wcs(self, i, x, y):
        """Convert coordinate in pixel into WCS

        Args:
            i (int): map index
            x (int): map x-coordinate
            y (int): map y-coordinate

        Returns:
            tuple: coordinates in WCS
        """
        if self.data_type == 'split':
            if self.is_ifu_mode:
                result = self.maps[i].wcs_coord[0].pixel_to_world_values(x, y)
            else:
                result = self.maps[i].wcs_coord[int(x)].pixel_to_world_values(0, y)
            return (result * u.deg).to(u.arcsec).value
        elif self.data_type == 'unsplit':
            return x*self.scale_x, y*self.scale_y

    def coord_px_to_arcsec(self, x, y):
        """Convert pixel coordinates into arcseconds using map pixel scale."""
        return x * self.scale_x, y * self.scale_y

    def get_timestamp(self, i, x, y):
        """Get timestamp for a given map index and pixel coordinates (each slit
        position has different observation time."""
        if self.data_type == 'split':
            if self.is_ifu_mode:
                return self.maps[i].timestamps[0].split('T')[1][:-3]
            else:
                return self.maps[i].timestamps[int(x)].split('T')[1][:-3]
        elif self.data_type == 'unsplit':
            return self.maps[i].timestamps[int(x)]

    def get_datetime(self, i, x, y):
        """Get timestamp as datetime for a given map index and pixel coordinates."""
        from dateutil.parser import parse
        dt = None
        try:
            dt = parse(self.maps[i].timestamps[int(x)])
        except:
            show_error('Cannot parse header timestamp to datetime')
        return dt


class GrisInvMaps:
    """Class for GRIS inversion parameters maps."""
    def __init__(self, file=None):
        self.num_x = 0
        self.num_y = 0
        self.num_pars = 0
        self.pars = []
        self.par_names = []
        self.data = []
        self.headers = []
        self.obs_name = ''
        self.file = file

    def read(self):
        """Read inversion map file."""
        try:
            fits_hdu = fitsio.open(self.file, memmap=False)
            self.num_pars = len(fits_hdu)
            data_shape = fits_hdu[0].data.shape
            self.num_x = data_shape[2]
            self.num_y = data_shape[1]
            self.data = [np.flip(hdu.data, axis=1) for hdu in fits_hdu]
            self.headers = [hdu.header for hdu in fits_hdu]
            self.obs_name = 'gris_' + self.headers[0]['POINT_ID']

            for i in range(self.num_pars):
                ext = self.headers[i]['EXTNAME']
                name = ' '.join(ext.split('_')[2:]).title()
                unit = self.headers[i]['BUNIT']
                scale = 0
                if name == 'Intensity':
                    scale = -3
                if name == 'Vlos':
                    scale = -5
                self.pars.append(PlotParam(name=name, unit=unit, scale=scale))
                self.par_names.append(name)
        except:
            self.data = []
            return


class GrisInvProfile:
    """Class for GRIS inverted line profile maps.
    Reads only fitted profiles, dropping out the input data."""
    def __init__(self, file=None):
        self.num_x = 0
        self.num_y = 0
        self.num_wl = 0
        self.num_pars = 0
        self.wl_unit = u.angstrom
        self.wl_scale = []
        self.obs_name = ''
        self.data = []
        self.headers = []
        self.file = file

    def read(self):
        """Read inversion line profiles file."""
        try:
            fits_hdu = fitsio.open(self.file, memmap=False)
            data_shape = fits_hdu[0].data.shape
            self.num_x = data_shape[2]
            self.num_y = data_shape[1]
            self.num_wl = data_shape[0]
            self.num_pars = 0
            self.data = []
            self.headers = []

            for hdu in fits_hdu:
                if 'Fit' in hdu.header['EXTNAME']:
                    self.num_pars += 1
                    self.data.append(np.flip(hdu.data, axis=1))
                    self.headers.append(hdu.header)
            self.obs_name = 'gris_' + self.headers[0]['POINT_ID']
            self.set_wl_scale()
        except:
            self.data = []
            return

    def set_wl_scale(self, unit=u.angstrom):
        """Retrieve profile wavelengths [in A or nm]."""
        hdr = self.headers[0]
        pts = np.arange(self.num_wl, dtype=np.float64)
        idx0 = hdr['WAVERPIX']
        wl0 = hdr['WAVERVAL']
        wl_samp = hdr['WAVEDELT']
        self.wl_scale = wl0 + (pts + 1 - idx0) * wl_samp
        if unit != u.angstrom:
            self.wl_scale = (self.wl_scale * u.angstrom).to(unit)
