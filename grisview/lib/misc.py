from PyQt6.QtWidgets import QMessageBox 
from pathlib import Path


def show_error(text):
    """Show error message box."""
    dlg = QMessageBox()
    dlg.setWindowTitle('Error')
    dlg.setText(text)
    dlg.setStandardButtons(QMessageBox.StandardButton.Ok)
    dlg.setIcon(QMessageBox.Icon.Critical)
    btn = dlg.exec()


def show_warning(text):
    """Show warning message box."""
    dlg = QMessageBox()
    dlg.setWindowTitle('Warning')
    dlg.setText(text)
    dlg.setStandardButtons(QMessageBox.StandardButton.Ok)
    dlg.setIcon(QMessageBox.Icon.Warning)
    btn = dlg.exec()


def show_info(text):
    """Show information message box."""
    dlg = QMessageBox()
    dlg.setWindowTitle('Information')
    dlg.setText(text)
    dlg.setStandardButtons(QMessageBox.StandardButton.Ok)
    dlg.setIcon(QMessageBox.Icon.Information)
    btn = dlg.exec()


def show_question_yes(text):
    """Show warning message box with yes/no buttons and return result."""
    dlg = QMessageBox()
    dlg.setWindowTitle('Warning')
    dlg.setText(text)
    dlg.setStandardButtons(QMessageBox.StandardButton.Yes | \
                           QMessageBox.StandardButton.No)
    dlg.setIcon(QMessageBox.Icon.Question)
    btn = dlg.exec()
    return btn == QMessageBox.StandardButton.Yes


def format_size_bytes(size_bytes):
    """Format size in bytes in MB/GB format."""
    size_gb = size_bytes / 1024 / 1024 / 1024.0
    if size_gb < 1.0:
        return f'{size_gb * 1024:.2f} MB'
    else:
        return f'{size_gb:.2f} GB'
