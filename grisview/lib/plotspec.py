from PyQt6 import QtGui, QtCore
from PyQt6.QtGui import QAction
from PyQt6.QtCore import Qt, pyqtSignal, QObject
from PyQt6.QtWidgets import QMenu, QGraphicsLineItem
from pyqtgraph import InfiniteLine, PlotItem, PlotDataItem, mkPen, \
    mkColor, ViewBox, Point, LinearRegionItem, TextItem
from pyqtgraph import functions as fn

import numpy as np
from astropy import units as u

from grisview.lib.plotpars import *
from grisview.dat import spec_lines_db, default_continuum_regions


class GrisSpecLineLabel(QObject):
    """Class representing a spectral line on the plot: consists of
    vertical mark and text label."""

    def __init__(self, parent, name, wl, inten=None):
        """Initialize instance.

        Args:
            parent (GrisPlotSpec): parent spectrum plot
            name (str): spectral line name (e.g. Ca I, Telluric etc.)
            wl (float): spectral line wavelength
            inten (float, optional): spectral line intensity. Defaults to None.
        """
        super().__init__(None)
        self.name = name
        self.wl = wl
        self.wl0 = wl
        self.inten = inten
        self.parent = parent
        self.lbl = TextItem(name, color='k', anchor=(1.0, 0.5))
        self.lbl.setAngle(90)
        self.mark = QGraphicsLineItem()
        self.mark.setPen(mkPen(cosmetic=True, color='k'))
        self.parent.addItem(self.mark)
        self.parent.addItem(self.lbl)
        self.frac_dy = 0.02
        self.frac_len = 0.06

    def set_visible(self, flag):
        """Set line mark and label visibility."""
        self.lbl.setVisible(flag)
        self.mark.setVisible(flag)

    def set_color(self, color):
        """Set color for line mark and label."""
        self.lbl.setPen(color)
        self.mark.setPen(color)

    def set_pos(self, wl=None, inten=None):
        """Set spectral line on the plot.

        Args:
            wl (float, optional): wavelength (in current plot units). Defaults to None.
            inten (float, optional): intensity at a given wavelength on the spectrum. Defaults to None.
        """
        if wl is None:
            wl = self.wl
        else:
            self.wl = wl
        if inten is None:
            inten = self.inten
        else:
            self.inten = inten
        if wl is not None and inten is not None:
            self.update_pos()

    def update_pos(self):
        """Update spectral line position on the plot."""
        vr = self.parent.viewRange()[1]
        dy = self.frac_dy * (vr[1] - vr[0])
        ylen = self.frac_len * (vr[1] - vr[0])
        self.mark.setLine(self.wl, self.inten - dy, self.wl, self.inten - dy - ylen)
        self.lbl.setPos(self.wl, self.inten - 1.2 * dy - ylen)

    def remove(self):
        """Delete spectral line from the parent plot."""
        self.parent.removeItem(self.mark)
        self.parent.removeItem(self.lbl)


class GrisSpecRegion(LinearRegionItem):
    """Modified LinearRegionItem class with right-click menu."""
    
    sig_deleted = pyqtSignal(object)

    def __init__(self, parent, **kargs):
        super().__init__(**kargs)
        self.create_menu()
        self.mouseClickEvent = self.on_mouse_click
        self.parent = parent
        self.parent.addItem(self)
        # self.raiseContextMenu = self.raise_context_menu

    def create_menu(self):
        """Create widget context menu."""
        self.menu = QMenu()
        menu = self.menu
        act = QAction("Delete", menu)
        act.triggered.connect(self.delete_reg)
        menu.addAction(act)

    def raise_context_menu(self, ev):
        """Raise widget context menu."""
        pos = ev.screenPos()
        self.menu.popup(QtCore.QPoint(int(pos.x()), int(pos.y())))

    def on_mouse_click(self, ev):
        """Process mouse click event."""
        if ev.button() == QtCore.Qt.MouseButton.RightButton:
            self.raise_context_menu(ev)
        ev.accept()

    def delete_reg(self):
        """Emit signal to delete region from parent plot."""
        self.sig_deleted.emit(self)
        # self.parent.removeItem(self)
        # for i, r in enumerate(self.parent.cont_regs):
        #     if r == self:
        #         del self.parent.cont_regs[i]
        #         break


class GrisPlotSpec(PlotItem):
    """ Class for plotting spectra, with zoom, pan and cursor browsing. Includes
    multiple graphs support, fitting continuum to selected (reference) spectrum,
    relative and custom wavelength scale, line marks with labels and custom
    wavelength markers (with quick navigation).
    """
    sig_hover = pyqtSignal(float, float)
    sig_cursor_pos_changed = pyqtSignal(int)
    sig_mouse_release = pyqtSignal()
    sig_mouse_press = pyqtSignal()
    sig_contin_changed = pyqtSignal()

    def __init__(self, par, **kargs):
        super().__init__(**kargs)

        self.par = par
        self.title = None
        self.unit = u.pix
        self.label_x = ''
        self.label_y = ''
        self.label_style = {'color': 'k',
                            'font-size': '10pt'}

        self.data_cubes = {}
        self.graphs = {}
        self.contin_regs = []

        self.contin_fit_pars = dict(window=None,
                                    degree=0,
                                    mult=1.0)
        self.contin_func = None
        self.contin_data = None
        self.contin_graph = PlotDataItem(pen=mkPen(mkColor(50, 50, 50), width=1.25),
                                         antialias=True)
        self.contin_graph.setVisible(False)

        self.wl_wcs = None
        self.wl_scale = None
        self.wl_scale0 = None
        self.wl_unit = None
        self.wl_unit0 = u.angstrom
        self.wl_offset = 0.0
        self.use_wl_ref = False
        self.wl_ref = 0.0
        self.wl_ref0 = 0.0

        self.cursor = InfiniteLine(angle=90,
                                   pen=mkPen(mkColor(255, 27, 84), width=1),
                                   hoverPen=mkPen(mkColor(255, 27, 84), width=2),
                                   movable=True)
        self.wl_markers = []
        self.lines = []
        self.line_intens_thresh = 0.8
        self.line_markers = []
        self.line_labels = []
        self.lines_color = '#fabed4'
        self.hline = InfiniteLine(angle=0, pen=mkPen(color='k', style=QtCore.Qt.PenStyle.DashLine), movable=False)
        self.vline = InfiniteLine(angle=90, pen=mkPen(color='k', style=QtCore.Qt.PenStyle.DashLine), movable=False)
        self.selection = LinearRegionItem()
        self.is_select_active = False
        self.hover_lines_enabled = False
        self.is_fixed_view_to_cursor = False
        # self.is_cont_fit_mode = False
        self.lines_visible = False
        self.wl_markers_visible = True
        # self.cursor.setVisible(False)
        self.addItem(self.cursor)
        self.addItem(self.hline)
        self.addItem(self.vline)
        self.addItem(self.contin_graph)
        self._prepare()

    def _prepare(self):
        """ Set plot properties, borders, axes, ticks and connect events."""
        self.setContentsMargins(5, 5, 20, 5)
        self.vb.setBorder(color='k', width=1)
        self.enableAutoRange('y')
        self.hideButtons()

        self.setMenuEnabled(True)
        self.create_menu()

        self.sigYRangeChanged.connect(self.update_yrange)
        self.yrange_changed = False
        self.yrange_prev = [0, 0]
        self.vb.hoverEvent = self.proc_hover_event
        self.cursor.sigPositionChanged.connect(self.update_cursor_pos)
        self.vb.mouseClickEvent = self.on_mouse_click
        self.vb.mouseDragEvent = self.on_mouse_drag
        self.vb.sigYRangeChanged.connect(self.update_lines)
        self.cursor.setZValue(1.0)
        self.hline.setVisible(False)
        self.vline.setVisible(False)

        self.showAxis('right')
        self.showAxis('top')
        tickFont = QtGui.QFont()
        tickFont.setPointSize(10)
        for pos in ['left', 'right', 'top', 'bottom']:
            ax = self.getAxis(pos)
            ax.showLabel(False)
            ax.setPen(color='k', width=1)
            ax.setTextPen('k')
            ax.setStyle(tickLength=-10, tickFont=tickFont, showValues=False)
            if pos == 'left' or pos == 'bottom':
                ax.setStyle(showValues=True)

    def reset(self):
        """Reset plot data and parameters."""
        self.wl_scale = None
        self.wl_wcs = None
        self.wl_unit = None

        self.data_cubes = {}        
        self.contin_regs = []
        self.contin_fit_pars = dict(window=None,
                                    degree=0,
                                    mult=1.0)
        self.contin_func = None
        self.contin_data = None

        self.use_wl_ref = False
        self.wl_ref = 0.0
        self.wl_offset = 0.0

        self.reset_wl_markers()

    def create_menu(self):
        """Create right-click context menu."""
        self.vb.menu = QMenu()
        menu = self.vb.menu

        act = QAction("Autoscale Y Axis to Visible Data", menu)
        act.triggered.connect(lambda: self.fit_yrange_to_view(vis_only=True))
        menu.addAction(act)

        act = QAction("Autoscale Y Axis to All Data", menu)
        act.triggered.connect(lambda: self.fit_yrange_to_view(vis_only=False))
        menu.addAction(act)

        act = QAction("Zoom All to View", menu)
        act.triggered.connect(self.fit_all_to_view)
        menu.addAction(act)

    def raise_context_menu(self, ev):
        """Raise context menu."""
        pos = ev.screenPos()
        self.vb.menu.popup(QtCore.QPoint(int(pos.x()), int(pos.y())))

    def fit_yrange_to_view(self, vis_only=False):
        """Set y axis scale to its global or currenly visible range."""
        self.enableAutoRange('y')
        self.setAutoVisible(y=vis_only)
        if self.par.zero_centered:
            y1, y2 = self.viewRange()[1]
            ymax = max(abs(y1), abs(y2))
            # self.vb.blockSignals(True)
            self.setYRange(-ymax, ymax, padding=0.02)
            # self.vb.blockSignals(False)
        #     self.setAutoVisible(y=vis_only)
        #     self.enableAutoRange('y')

    def fit_all_to_view(self):
        """Set x and y axes scales to their full range."""
        self.fit_yrange_to_view(vis_only=False)
        self.enableAutoRange('x')

    def show_grid(self, flag=True):
        """Show/hide plot grid."""
        x_axis = self.getAxis('bottom')
        x_axis.setGrid(96 if flag else False)
        y_axis = self.getAxis('left')
        y_axis.setGrid(96 if flag else False)

    def add_graph(self, name, color=None):
        """Create new spectrum graph of given name and color."""
        if name not in self.data_cubes:
            self.graphs[name] = PlotDataItem(antialias=True)
            if color is not None:
                self.graphs[name].setPen(color)
            self.addItem(self.graphs[name])

    def set_data_cube(self, name, data):
        """Set data cube (IQUV) for selected spectrum graph name."""
        self.data_cubes[name] = data

    def has_data_cube(self, name):
        """Check if data cube for selected spectrum name is present."""
        if name not in self.data_cubes:
            return False
        else:
            return self.data_cubes[name] is not None

    def update_graph(self, name):
        """Update selected spectrum graph."""
        graph_data = self.get_graph_data(name)
        if graph_data is not None and \
           self.wl_scale is not None:
            self.graphs[name].setData(
                x=self.wl_scale,
                y=graph_data)
        else:
            self.graphs[name].setData()
        if self.lines_visible:
            self.update_lines()

    def get_graph_data(self, name):
        """Return data for selected spectrum graph."""
        if self.has_data_cube(name):
            if self.par.normalized:
                if self.contin_data is not None:
                    try:
                        return self.par.func(self.data_cubes[name], self.contin_data)
                    except:
                        return None
                else:
                    return None
            else:
                try:
                    return self.par.func(self.data_cubes[name])
                except:
                    return None
        else:
            return None

    def get_graph_style(self, name):
        """Return selected spectrum graph line color, width and style."""
        pen = self.graphs[name].opts['pen']
        return pen.color().name(), pen.widthF(), pen.style()

    def set_graph_style(self, name, color=None, width=None, linestyle=None):
        """Set selected spectrum line color, width and style."""
        if name in self.graphs:
            pen = self.graphs[name].opts['pen']
            if color is None:
                color = pen.color().name()
            if width is None:
                width = pen.widthF()
            if linestyle is None:
                linestyle = pen.style()
            self.graphs[name].setPen(color=color,
                                     width=width,
                                     style=linestyle)

    def set_graph_visible(self, name, flag):
        """Show/hide selected spectrum."""
        if name in self.graphs:
            self.graphs[name].setVisible(flag)

    def is_graph_visible(self, name):
        """Check if graph for selected spectrum name is visible."""
        if name in self.graphs:
            return self.graphs[name].isVisible()
        else:
            return False

    def has_continuum(self):
        """Check if continuum data is present."""
        return self.contin_data is not None

    def get_continuum_at(self, wl, wl_unit=None):
        """Get fitted continuum value for a given wavelength.

        Args:
            wl (float): wavelength value
            wl_unit (str, optional): wavelength unit. Defaults to None.

        Returns:
            float: continuum intensity
        """
        if self.has_continuum():
            if wl_unit is None:
                wl_unit = self.wl_unit
            wl = self.convert_wl(wl, wl_unit, self.wl_unit)
            return self.contin_func(wl) * self.contin_fit_pars['mult']
        else:
            return None

    def fit_continuum(self, name_ref='Quiet Sun', degree=None, window=None, mult=None):
        """Perform continuum fitting to the spectrum.

        Args:
            name_ref (str, optional): name of the reference data cube. Defaults to 'Quiet Sun'.
            degree (int, optional): degree of Chebyshev polynomial. Defaults to None.
            window (list, optional): array of wavelength regions to perform fitting. Defaults to None.
            mult (float, optional): multiplier to shift fit up/down. Defaults to None.
        """
        self.contin_func = None
        self.contin_data = None
        try:
            if self.has_data_cube(name_ref) and \
               self.wl_scale is not None:

                x = np.copy(self.wl_scale)
                if self.use_wl_ref:
                    x += self.wl_ref
                x0 = x
                y = self.data_cubes[name_ref][0]
                pars = self.contin_fit_pars

                if degree is not None:
                    pars['degree'] = degree
                pars['window'] = None
                if window is not None:
                    pars['window'] = window
                else:
                    if self.contin_regs:
                        pars['window'] = []
                        for reg in self.contin_regs:
                            pars['window'].append(reg.getRegion())
                if mult is not None:
                    pars['mult'] = mult
                if pars['window'] is not None:
                    xr, yr = [], []
                    for w in pars['window']:
                        x1, x2 = w
                        if self.use_wl_ref:
                            x1 += self.wl_ref
                            x2 += self.wl_ref
                        sel = np.where((x >= x1) & (x <= x2))
                        xr = np.append(xr, x[sel])
                        yr = np.append(yr, y[sel])
                    x, y = xr, yr
                self.contin_func = np.polynomial.Chebyshev.fit(x, y, pars['degree'])
                self.contin_data = self.contin_func(x0) * pars['mult']
                self.contin_graph.setData(self.wl_scale, self.contin_data)
        except:
            print('Cannot fit continuum')

        self.sig_contin_changed.emit()

    def convert_wl(self, val, unit_from, unit_to):
        """Convert wavelength value (or array of values) from one unit
        to another."""
        if unit_from == unit_to:
            return val
        if unit_from == u.pix:
            wl0 = (self.wl_wcs.wcs_pix2world(val, 0)[0] * u.m).to(unit_to).value
            wl_off = (self.wl_offset * self.wl_unit0).to(unit_to).value
            return wl0 + wl_off
        elif unit_to == u.pix:
            wl_off = (self.wl_offset * self.wl_unit0).to(unit_from)
            wl0 = (val * unit_from - wl_off).to(u.m)
            res = self.wl_wcs.wcs_world2pix(wl0, 0)
            return int(res[0])
        else:
            return (val * unit_from).to(unit_to).value

    def set_wl_scale_unit(self, unit):
        """Set wavelength scale unit and update."""
        self.wl_unit = unit
        self.update_wl_scale()

    def set_wl_scale_ref(self, flag=True):
        """Set relative wavelength scale at cursor position."""
        self.use_wl_ref = flag
        if flag:
            self.wl_ref = self.convert_wl(
                self.cursor.value(), self.wl_unit, self.wl_unit0)
            self.wl_ref0 = self.wl_ref - self.wl_offset
        else:
            self.wl_ref = 0.0
            self.wl_ref0 = 0.0
        self.update_wl_scale()

    def set_wl_scale_offset(self, offset):
        """Set wavelength scale offset in Angstroms."""
        self.wl_offset = offset
        if self.use_wl_ref:
            self.wl_ref = self.wl_ref0 + self.wl_offset
        self.update_wl_scale()

    def set_wl_scale(self, wl, unit=None, wcs=None):
        """Init wavelength scale and unit using provided WCS or a predefined wl array."""
        if wcs is not None:
            self.wl_wcs = wcs
        if unit is not None:
            self.wl_unit0 = unit
        if type(wl) == int:
            # wl equals to num. of wavelength
            self.wl_scale0 = np.arange(wl, dtype=np.float64)
            if self.wl_wcs is not None:
                self.wl_scale0 = self.convert_wl(self.wl_scale0, u.pix, self.wl_unit0)
        else:
            # wl is a list of ready wavelength values
            self.wl_scale0 = wl
        self.wl_scale = self.wl_scale0
        self.wl_unit = self.wl_unit0
        self.setXRange(self.wl_scale[0], self.wl_scale[-1], padding=0)

    def update_wl_scale(self):
        """Init/update wavelength axis according to current WCS, unit,
        reference wavelength and update cursor position."""
        wl1, wl2 = self.wl_scale[0], self.wl_scale[-1]
        self.wl_scale = self.wl_scale0 + self.wl_offset - self.wl_ref
        self.wl_scale = self.convert_wl(self.wl_scale, self.wl_unit0, self.wl_unit)
        wl1n, wl2n = self.wl_scale[0], self.wl_scale[-1]

        # translate x from old to new scale
        transl = lambda x: (wl1n * (wl2 - x) + wl2n * (x - wl1)) / (wl2 - wl1)

        # update plot range and limits
        self.setLimits(xMin=wl1n, xMax=wl2n)
        wl1_vb, wl2_vb = self.viewRange()[0]
        self.setXRange(transl(wl1_vb), transl(wl2_vb), padding=0)

        # update line markers
        for mrk in self.line_markers:
            mrk.wl = self.convert_wl(mrk.wl0 - self.wl_ref, self.wl_unit0, self.wl_unit)

        # update wavelength axis markers
        for mrk in self.wl_markers:
            mrk.setValue(transl(mrk.value()))

        # update cursor position
        self.cursor.blockSignals(True)
        wl_cur = self.cursor.value()
        self.cursor.setBounds([wl1n, wl2n])
        self.cursor.setValue(transl(wl_cur))
        self.cursor.blockSignals(False)

        # update continuum regions
        if self.contin_regs:
            for reg in self.contin_regs:
                wl1r, wl2r = reg.getRegion()
                reg.blockSignals(True)
                reg.setBounds([wl1n, wl2n])
                reg.setRegion([transl(wl1r), transl(wl2r)])
                reg.blockSignals(False)

        #update graphs
        for name in self.graphs:
            self.update_graph(name)

        self.set_x_label()

    def set_default_cont_regs(self):
        """Set/reset continuum fitting wavelength regions to the predefined ones."""
        for reg in self.contin_regs:
            self.removeItem(reg)
        self.contin_regs = []
        for intvl in default_continuum_regions:
            wl1r, wl2r = self.convert_wl(intvl.value, intvl.unit, self.wl_unit)
            wl_off = (self.wl_offset * self.wl_unit0).to(self.wl_unit).value
            wl1r += wl_off
            wl2r += wl_off
            wl1, wl2 = self.wl_scale[0], self.wl_scale[-1]
            if wl1 <= wl1r <= wl2 and \
               wl1 <= wl2r <= wl2:
                self.contin_regs.append(GrisSpecRegion(values=[wl1r, wl2r], parent=self))
        for reg in self.contin_regs:
            reg.sigRegionChanged.connect(lambda: self.fit_continuum())
            reg.sig_deleted.connect(self.delete_cont_reg)
        self.fit_continuum()

    def add_cont_reg(self):
        """Add new continuum fitting wavelength region in the middle of the
        current view."""
        x1, x2 = self.viewRange()[0]
        f = 0.05  # fraction of viewbox region
        wl1r = 0.5 * (x1 * (1 + f) + x2 * (1 - f))
        wl2r = 0.5 * (x1 * (1 - f) + x2 * (1 + f))
        self.contin_regs.append(GrisSpecRegion(values=[wl1r, wl2r], parent=self))
        self.contin_regs[-1].sigRegionChanged.connect(lambda: self.fit_continuum())
        self.contin_regs[-1].sig_deleted.connect(self.delete_cont_reg)
        self.fit_continuum()

    def delete_cont_reg(self, reg):
        """Delete selected continuum fitting wavelength region."""
        self.removeItem(reg)
        for i, r in enumerate(self.contin_regs):
            if r == reg:
                del self.contin_regs[i]
                break
        self.fit_continuum()

    def show_cont_fit_regs(self, flag):
        """Show/hide continuum fitting wavelength regions."""
        for reg in self.contin_regs:
            reg.setVisible(flag)
        self.contin_graph.setVisible(flag)

    def add_wl_marker(self, wl=None):
        """Add new wavelength marker."""
        if wl is None:
            wl = self.cursor.value()
        if len(self.wl_markers) > 0:
            mark_vals = [m.value() for m in self.wl_markers]
            uniq = wl not in mark_vals
        else:
            uniq = True
        if uniq:
            self.wl_markers.append(InfiniteLine(pos=wl, angle=90,
                                                pen=mkPen('#3cb44b', width=1, # '#b3b3b3'
                                                          style=QtCore.Qt.PenStyle.DashLine
                                                #          , cosmetic=True
                                                          ),
                                                movable=False))
            self.wl_markers[-1].setZValue(0.5)
            self.wl_markers[-1].setVisible(self.wl_markers_visible)
            self.addItem(self.wl_markers[-1])
            self.wl_markers = sorted(self.wl_markers, key=lambda x: x.value())
        return uniq

    def delete_wl_marker(self, idx):
        """Delete wavelength marker of given index."""
        self.removeItem(self.wl_markers[idx])
        del self.wl_markers[idx]

    def reset_wl_markers(self):
        """Delete and reset all """
        for mrk in self.wl_markers:
            self.removeItem(mrk)
        self.wl_markers = []

    def hide_wl_markers(self, flag=True):
        self.wl_markers_visible = not flag
        for mrk in self.wl_markers:
            mrk.setVisible(not flag)

    def init_lines(self):
        if self.par.name == ParIntensity or self.par.name == ParIntensityNorm:
            if self.wl_scale is not None:
                # Input line db has wl in angstroms
                wl1 = self.convert_wl(self.wl_scale[0], self.wl_unit, u.angstrom)
                wl2 = self.convert_wl(self.wl_scale[-1], self.wl_unit, u.angstrom)
                self.line_markers = []
                for sp in spec_lines_db:
                    if wl1 < sp['wl'] < wl2:
                        self.line_markers.append(
                            GrisSpecLineLabel(parent=self, name=sp['name'], wl=sp['wl']))
                        self.line_markers[-1].set_visible(self.lines_visible)

    def show_lines(self, flag=True):
        if self.line_markers:
            self.lines_visible = flag
            for m in self.line_markers:
                m.set_visible(flag)
            if self.lines_visible:
                self.update_lines()

    def update_lines(self):
        if self.wl_scale is not None:
            for mrk in self.line_markers:
                idx_wl = np.abs(self.wl_scale - mrk.wl).argmin()
                ymin_wl = np.inf
                for name in self.graphs:
                    if self.is_graph_visible(name) and \
                       self.has_data_cube(name):
                        try:
                            # ymin_wl = min(ymin_wl, self.graphs[name].yDisp[idx_wl])
                            yval = self.graphs[name].getData()[1][idx_wl]
                            ymin_wl = min(ymin_wl, yval)
                        except:
                            continue
                mrk.set_pos(inten=ymin_wl)

    def update_lines_pos(self):        
        for mrk in self.line_markers:
            mrk.update_pos()

    def goto_line(self, idx):
        """Move cursor to the spectral line of given index."""
        self.cursor.setValue(self.line_markers[idx].wl)
        if self.is_fixed_view_to_cursor:
            self.move_view_to_cursor()

    def goto_next_line(self):
        """Move cursor to closest next spectral line."""
        idx = -1
        if self.line_markers:
            val = self.cursor.value()
            mark_vals = np.array([m.wl for m in self.line_markers])
            if val < mark_vals[-1]:
                idx = np.searchsorted(mark_vals, val, side='right')
                self.goto_line(idx)
        return idx

    def goto_prev_line(self):
        """Move cursor to closest previous spectral line."""
        idx = -1
        if self.line_markers:
            val = self.cursor.value()
            mark_vals = np.array([m.wl for m in self.line_markers])
            if val > mark_vals[0]:
                idx = np.searchsorted(mark_vals, val, side='left') - 1
                self.goto_line(idx)
        return idx

    def set_lines_color(self, c):
        """Set spectral lines color."""
        self.lines_color = c
        for m in self.line_markers:
            m.setPen(c)

    def goto_marker(self, idx):
        """Go to the user wavelength marker of the given index."""
        self.cursor.setValue(self.wl_markers[idx].value())
        if self.is_fixed_view_to_cursor:
            self.move_view_to_cursor()

    def goto_next_marker(self):
        """Go to next user wavelength marker."""
        idx = -1
        if self.wl_markers:
            val = self.cursor.value()
            mark_vals = np.array([m.value() for m in self.wl_markers])
            if val < mark_vals[-1]:
                idx = np.searchsorted(mark_vals, val, side='right')
                self.goto_marker(idx)
        return idx

    def goto_prev_marker(self):
        """Go to previous user wavelength marker."""
        idx = -1
        if self.wl_markers:
            val = self.cursor.value()
            mark_vals = np.array([m.value() for m in self.wl_markers])
            if val > mark_vals[0]:
                idx = np.searchsorted(mark_vals, val, side='left') - 1
                self.goto_marker(idx)
        return idx

    def str_wl_unit(self):
        """Get current wavelength unit as string."""
        res = ''
        if self.wl_unit == u.pix:
            res = 'index'
        elif self.wl_unit == u.angstrom:
            res = 'Å'
        elif self.wl_unit == u.nm:
            res = 'nm'
        return res

    def str_wl(self, val, unit=None):
        """Convert wavelength as string with a given unit."""
        if unit is None:
            unit = self.wl_unit
        if unit == u.pix:
            return f'{val:.0f} px'
        elif unit == u.angstrom:
            return f'{val:.3f} Å'
        elif unit == u.nm:
            return f'{val:.4f} nm'

    def set_x_label(self, label=None):
        """Set X axis label with unit."""
        if label is not None:
            self.label_x = label
        ax = self.getAxis('bottom')
        txt = self.label_x
        if self.use_wl_ref:
            ref = self.convert_wl(self.wl_ref, self.wl_unit0, self.wl_unit)
            if self.wl_unit == u.pix:
                txt = f'i - {ref:.0f}'
            else:
                txt = '\u03BB' + f' - {ref:.3f}'
        u_str = self.str_wl_unit()
        # ax.showLabel(False)
        ax.setLabel('')
        if u_str:
            ax.setLabel(txt + f' [{u_str}]', **self.label_style)
        else:
            ax.setLabel(txt, **self.label_style)
        # ax.showLabel(True)

    def set_y_label(self, par='', unit='', scale=0):
        """Set Y axis label using parameter name, scale and unit."""
        txt = ''
        if par:
            txt = par + ' '
        if scale != 0 and unit:
            txt += f"[×10<sup>{-scale:.0f}</sup> {unit}]"
        elif scale !=0:
            txt += f"[×10<sup>{-scale:.0f}</sup>]"
        elif unit:
            txt += f"[{unit}]"
        self.label_y = txt
        ax = self.getAxis('left')
        ax.setLabel(self.label_y, **self.label_style)
        ax.setScale(10**scale)

    def show_cursor(self, flag=True):
        """Show/hide cursor."""
        self.cursor.setVisible(flag)

    def set_cursor_pos(self, wl_pix):
        """Set cursor position as wavelength array index value (no units)."""
        val = self.convert_wl(wl_pix, u.pix, self.wl_unit)
        if self.use_wl_ref:
            val -= self.convert_wl(self.wl_ref, self.wl_unit0, self.wl_unit)
        # self.cursor.blockSignals(True)
        self.cursor.setValue(val)
        if self.is_fixed_view_to_cursor:
            self.move_view_to_cursor()
        # self.cursor.blockSignals(False)

    def get_cursor_pos_str(self):
        """Get cursor position with the unit as string."""
        val = self.cursor.value()
        return f'{val:.3f} {self.str_wl_unit()}'

    def on_mouse_click(self, ev):
        """Process mouse click event."""
        if ev.button() == QtCore.Qt.MouseButton.LeftButton:
            p = self.vb.mapToView(ev.pos())
            val = p.x()
            self.cursor.setValue(val)            
        if ev.button() == QtCore.Qt.MouseButton.RightButton:
            self.raise_context_menu(ev)
        ev.accept()

    def update_cursor_pos(self):
        """Update vertical cursor position."""
        val = self.cursor.value()
        if self.use_wl_ref:
            val += self.convert_wl(self.wl_ref, self.wl_unit0, self.wl_unit)
        if self.wl_wcs:
            val = self.convert_wl(val, self.wl_unit, u.pix)
        self.sig_cursor_pos_changed.emit(int(val))

    def move_view_to_cursor(self):
        """Move view to have cursor in the center."""
        wl1 = self.viewRange()[0][0]
        wl2 = self.viewRange()[0][1]
        d = wl2 - wl1
        wl1 = max(self.wl_scale[0], self.cursor.value() - 0.5*d)
        wl2 = wl1 + d
        if wl2 > self.wl_scale[-1]:
            wl2 = self.wl_scale[-1]
            wl1 = wl2 - d
        self.setXRange(wl1, wl2, padding=0)

    def move_cursor_to_view(self):
        """Move cursor to the center of the current view."""
        wl1 = self.viewRange()[0][0]
        wl2 = self.viewRange()[0][1]
        self.cursor.setValue(0.5*(wl1 + wl2))

    def zoom_in_horiz(self):
        """Zoom in X axis."""
        self.vb.scaleBy(x=0.909)

    def zoom_out_horiz(self):
        """Zoom out X axis."""
        self.vb.scaleBy(x=1.1)

    def zoom_all_horiz(self):
        """Zoom to X range."""
        self.setXRange(self.wl_scale[0], self.wl_scale[-1], padding=0.01)

    def show_hover_lines(self, flag=True):
        """Show/hide horizontal and vertical lines moving with mouse cursor
        while hovering."""
        self.hover_lines_enabled = flag

    def proc_hover_event(self, ev):
        """Process hover event and emit signal with current cursor plot coordinates."""
        if ev.isExit():
            self.sig_hover.emit(np.inf, np.inf)
            self.hline.setVisible(False)
            self.vline.setVisible(False)
            return
        p = self.vb.mapToView(ev.pos())
        if self.hover_lines_enabled:
            self.hline.setVisible(True)
            self.vline.setVisible(True)
            self.hline.setValue(p.y())
            self.vline.setValue(p.x())
        self.sig_hover.emit(p.x(), p.y())

    def update_yrange(self):
        """Custom Y axis range updating method to account for zero-centered
        plots."""
        if self.par.zero_centered:
            # flag = self.vb.autoRangeEnabled()
            # if not self.yrange_changed:
            # if self.vb.autoRangeEnabled():
            # if self.yrange_prev != self.viewRange()[1]:
            y1, y2 = self.viewRange()[1]
            ymax = max(abs(y1), abs(y2))
            self.blockSignals(True)
            # self.vb.setYRange(-ymax, ymax)
            self.setYRange(-ymax, ymax)
            # self.vb.state['autoRange'][1] = True
            # self.getAxis('left').setRange(-ymax, ymax)
            # self.yrange_prev = self.viewRange()[1]
            # self.enableAutoRange('y')
            # self.vb.state['autoRange'][1] = True
            self.blockSignals(False)
            self.sigYRangeChanged.disconnect(self.update_yrange)

    def on_mouse_drag(self, ev, axis=None):
        """Modified ViewBox.mouseDragEvent method for zoom region selection and
        handling zero-centered plots (such as Stokes Q,U,V)."""
        if ev.button() & QtCore.Qt.MouseButton.LeftButton:
            if ev.modifiers() == Qt.KeyboardModifier.ShiftModifier:
                if not self.is_select_active:
                    self.is_select_active = True
                    self.vb.setMouseMode(ViewBox.RectMode)

        if not self.par.zero_centered:
            ViewBox.mouseDragEvent(self.vb, ev)
        else:
            if not self.is_select_active:
                # custom drag event processing for zero centered plot
                if ev.button() & (QtCore.Qt.MouseButton.LeftButton | QtCore.Qt.MouseButton.MiddleButton):
                    dif = ev.lastPos() - ev.pos()
                    tr = self.vb.childGroup.transform()
                    tr = fn.invertQTransform(tr)
                    tr = tr.map(dif) - tr.map(Point(0, 0))
                    x = tr.x()
                    self.vb._resetTarget()
                    self.vb.translateBy(x=x)
                    self.vb.sigRangeChangedManually.emit(self.vb.state['mouseEnabled'])
                elif ev.button() & QtCore.Qt.MouseButton.RightButton:
                    dif = ev.screenPos() - ev.lastScreenPos()
                    dif = np.array([dif.x(), dif.y()])
                    dif[0] *= -1
                    x, y = ((np.array([1, 1]) * 0.02) + 1) ** dif
                    self.vb._resetTarget()
                    self.vb.scaleBy(x=x, y=y)
                    self.vb.sigRangeChangedManually.emit(self.vb.state['mouseEnabled'])

        if self.is_select_active:
            # stick selection region to vertical range
            p1 = Point(ev.buttonDownPos()[0], 0)
            p2 = Point(ev.pos()[0], self.vb.screenGeometry().height())
            self.vb.updateScaleBox(p1, p2)

        if ev.isFinish() and self.is_select_active:
            # apply modified selection region zooming
            self.is_select_active = False
            self.vb.rbScaleBox.hide()
            self.vb.setMouseMode(ViewBox.PanMode)
            ax = QtCore.QRectF(Point(ev.buttonDownPos(ev.button())[0], 0),
                               Point(ev.pos()[0], self.vb.screenGeometry().height()))
            ax = self.vb.childGroup.mapRectFromParent(ax)
            self.vb.showAxRect(ax)
            self.vb.axHistoryPointer += 1
            self.vb.axHistory = self.vb.axHistory[:self.vb.axHistoryPointer] + [ax]

        ev.accept()

    def link_view_to(self, plt=None, x=True, y=False):
        """Link plot view to selected plot instance (or unlink if None).
        X and Y ranges can be linked separately."""
        if plt is not None:
            if x:
                self.vb.setXLink(plt.vb)
            if y:
                self.vb.setYLink(plt.vb)
        else:
            self.vb.setXLink(None)
            self.vb.setYLink(None)

    def _sync_cursor_pos(self, cur):
        """Sync cursor position to the given cursor."""
        self.cursor.blockSignals(True)
        self.cursor.setValue(cur.value())
        self.cursor.blockSignals(False)

    def link_cursor_to(self, plt):
        """Link cursor to autosync with another plot cursor position."""
        self.cursor.sigPositionChanged.connect(plt._sync_cursor_pos)
        plt.cursor.sigPositionChanged.connect(self._sync_cursor_pos)
