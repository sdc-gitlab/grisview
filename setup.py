"""Setup for the biospy package."""
from setuptools import setup, find_packages
from glob import glob
from os.path import join

import setuptools

with open('README.md') as f:
    README = f.read()
with open('requirements.txt', 'r') as req_file:
    requirements = [line.strip() for line in req_file]

setup(
    name='grisview',
    version="0.8.6",
    author="Taras Yakobchuk",
    author_email="yakobchuk@leibniz-kis.de",        
    description='GRIS instrument data visualization software package',
    license="MIT",
    long_description=README,
    url='https://gitlab.leibniz-kis.de/sdc/gris/grisview',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requirements,
    scripts=glob(join("bin", "*")),
    classifiers=[
        # Trove classifiers
        # (https://pypi.python.org/pypi?%3Aaction=list_classifiers)
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Intended Audience :: Developers',
    ],
    platforms="any",
    python_requires=">=3.8,<4.0",
    entry_points="""
        [console_scripts]
        grisview=grisview:main
      """,
)
